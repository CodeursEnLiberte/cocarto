module MonkeyPatches
  module ActionViewTemplateStrictLocalsWithRequestId
    # Workaround a bug between rails-turbo and rails’ has_strict_locals
    # Since turbo-rails #499 2.0.0-beta1 (https://github.com/hotwired/turbo-rails/releases?page=2
    # - https://github.com/hotwired/turbo-rails/pull/499
    # - https://github.com/hotwired/turbo-rails/commit/7800f38ee94247bd28362a4fd9de850b66ee0858#diff-5956b6256febdc6ef2d3a5fe18f05cc7449578d891022b1d5d439fba803643c2R427)
    # broadcast_rendering_with_defaults implicitly adds a `:request_id` local to all views rendered for turbo.
    # This clashes with ActionView::Template management of strict locals
    # - https://github.com/rails/rails/blob/161d98dec2706521cb5d1f58acd87c3c1f391c8a/actionview/lib/action_view/template.rb#L357C9-L357C22
    # - https://github.com/rails/rails/blob/161d98dec2706521cb5d1f58acd87c3c1f391c8a/actionview/lib/action_view/template.rb#L453
    # As :request_id is not referenced in the magic comment that gets converted to proper argument in the compiled view,
    # this triggers a `ActionView::Template::Error: unknown local: :request_id`.
    # We workaround it by adding an optional request_id param to the parsed magic comment.
    def strict_locals!
      locals = super
      if locals.nil? || locals == "**nil"
        locals
      else
        locals + ", request_id: nil"
      end
    end
  end
end
