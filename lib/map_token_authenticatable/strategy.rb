# The Warden Strategy; called by warden if a request is made, after the regular strategy.
class MapTokenAuthenticatable::Strategy < ::Warden::Strategies::Base
  def valid?
    params.key?(:token) || params.key?(:authkey) || request.headers.key?("X-Auth-Key")
  end

  def authenticate!
    api_token = params[:authkey] || request.headers["X-Auth-Key"]
    user_token = params[:token]
    token = user_token || api_token

    map_token = MapToken.find_by(token: token)
    if map_token.nil?
      fail!(I18n.t("api.bad_key"))
      return
    end

    user = MapTokenAuthenticatable::AnonymousUser.new
    user.store_tokens_array_in_session(session)
    user.assign_map_token(map_token)
    success!(user)
  end
end
