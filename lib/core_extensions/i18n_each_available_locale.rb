module CoreExtensions
  # Extends I18n
  module I18nEachAvailableLocale
    # loop over and yield each available locale
    def each_available_locales(&block)
      available_locales.each do |tmp_locale|
        with_locale(tmp_locale) do
          yield(tmp_locale)
        end
      end
    end
  end
end
