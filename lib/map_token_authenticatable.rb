# Handle User authentication via map token.
# Enables anonymous access to the map page as well as API access to the exported layers. (e.g. geojson)
#
# If the passed token is found, an MapTokenAuthenticatable::AnonymousUser is set up but it is not saved in the database,
# only in the session cookie.
# The map_token is also added to another key of the session cookie.
# Several map_tokens can be added like this; an anonymous user can access several maps,
# and when converting to a real account or signing in, we can reassign these tokens to user_roles.
#
# See also MapsController#shared and User#assign_map_token.
# See also ApplicationController#restore_anonymous_session and ApplicationCable::Connection#find_verified_user
module MapTokenAuthenticatable
  extend ActiveSupport::Concern

  ## Overrides for the User class itself
  included do
    def anonymous? = false

    def reassign_from_anonymous_user(anonymous_user)
      accessed_map_tokens = MapToken.where(id: anonymous_user.map_tokens).includes(:map)
      accessed_map_tokens.each { |map_token| assign_map_token(map_token) }

      rows = Row.where(anonymous_tag: anonymous_user.anonymous_tag)
      rows.update(anonymous_tag: nil, author_id: id)
    end
  end

  module ClassMethods
    # Session serialization overload: we use the anonymous_tag to identify an AnonymousUser in the next request.
    # (This is stored in the "warden.user.user.key" key of the session cookie).
    COCARTO_ANONYMOUS = "cocarto_anonymous"

    def serialize_from_session(*args)
      return super unless args[0] == COCARTO_ANONYMOUS

      AnonymousUser.new(args[1])
    end

    def serialize_into_session(user)
      return super unless user.anonymous?

      [COCARTO_ANONYMOUS, user.anonymous_tag]
    end
  end
end
