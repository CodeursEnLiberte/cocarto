class ApplicationController < ActionController::Base
  # Pundit handles the authorization policies
  include Pundit::Authorization
  # Note: we can’t use only: :action or except: :action in the after_action hooks below because of raise_on_missing_callback_actions.
  # However, we still want these hooks by default for all regular routes.
  after_action :verify_authorized, unless: -> { _1.action_name == "index" }
  after_action :verify_policy_scoped, if: -> { _1.action_name == "index" }
  after_action :record_page_view

  around_action :switch_locale, # make sure set_locale is first so that error messages are localized
    :restore_anonymous_session,
    :set_sentry_user,
    :rescue_unauthorized

  def default_url_options
    {locale: I18n.locale}
  end

  def switch_locale(&action)
    locale = params.delete(:locale) || http_accept_language.compatible_language_from(I18n.available_locales)
    I18n.with_locale(locale, &action)
  end

  def restore_anonymous_session # See MapTokenAuthenticatable
    if warden.authenticated? && current_user.anonymous?
      current_user.store_tokens_array_in_session(session) # restore anonymous map tokens
    end
    yield
  end

  def set_sentry_user
    # Note: this is where we call `current_user` for the first time in each request.
    # It has side effects!
    # (e.g. if the found user is invited but not signed up yet)
    # See the Devise::FailureApp for flashes and redirections.
    if warden.authenticated?
      Sentry.set_user(id: helpers.current_user_identifier)
    end
    yield
  end

  def rescue_unauthorized
    yield
  rescue Pundit::NotAuthorizedError
    flash[:alert] = t("common.user_not_authorized")
    if request.get? && request.referer != request.url
      redirect_back_or_to(root_path)
    else
      head :not_found
    end
  end

  def record_page_view
    if response.content_type&.start_with?("text/html")
      ActiveAnalytics.record_request(request)
    end
  end
end
