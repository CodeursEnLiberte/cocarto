class MapsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_base_map_styles
  before_action :new_map, only: %i[new create]
  before_action :set_map, only: %i[show edit update destroy]

  def index
    @maps = policy_scope(Map).includes(:layers, :user_roles)
    @maps_rows_count = policy_scope(Map).joins(:rows).group("maps.id").count("rows.id")
    @maps_rows_count.default = 0

    @new_map = Map.new
    @new_map.user_roles.new(user: current_user, role_type: :owner)
  end

  def show
    @role_type = current_user.access_for_map(@map).role_type
    @initially_active_layer = @map.layers.find_by(id: params["open"])

    respond_show
  end

  def shared
    # Access made with a map token.
    # Authentication is already done by MapTokenAuthenticatable.
    map_token = MapToken.find_by(token: params[:token])
    current_user.assign_map_token(map_token)
    if current_user.anonymous?
      # Transient users only see the /share/:token url
      @map = authorize(map_token.map)
      @role_type = map_token.role_type

      respond_show
    else
      # Real users are redirected to /maps/:id
      redirect_to authorize(map_token.map)
    end
  end

  def new
    @map.layers.new
    render "form"
  end

  def edit
    render "form"
  end

  def create
    if @map.update(map_params)
      redirect_to map_path(@map)
    else
      # This line overrides the default rendering behavior, which
      # would have been to render the "create" view.
      render "new", status: :unprocessable_entity
    end
  end

  def update
    if @map.update(map_params)
      if [:default_latitude, :default_longitude, :default_zoom].any? { _1.in? @map.previous_changes }
        flash.now[:notice] = t("helpers.message.map.center_and_zoom_saved")
      end
      respond_to do |format|
        format.turbo_stream { render turbo_stream: [] }
        format.html { redirect_to map_path(@map) }
      end
    else
      render :show, status: :unprocessable_entity
    end
  end

  def destroy
    @map.destroy
    redirect_to maps_url, notice: t("helpers.message.map.destroyed"), status: :see_other
  end

  private

  def respond_show # Same response for maps#show and maps#shared
    respond_to do |format|
      format.html { render :show }
      format.style { render json: @map.style(url_for(Layer)) }
    end
  end

  def set_map
    @map = Map.find(params[:id])
    authorize @map
  end

  def new_map
    @map = Map.new
    @map.user_roles.new(user: current_user, role_type: :owner)
    authorize @map
  end

  def map_params
    params.require(:map).permit(:name, :default_latitude, :default_longitude, :default_zoom, :base_style_url,
      layers_attributes: [:name, :geometry_type, :map_id, :color, territory_category_ids: []])
  end

  def set_base_map_styles
    default_style = [[I18n.t("common.default_map_style"), ENV["DEFAULT_MAP_STYLE"] || "https://demotiles.maplibre.org/style.json"]]
    @map_styles = default_style + Rails.application.config.extra_map_styles.to_a
  end
end
