class Admin::UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :skip_policy_scope # We don’t use :policy_scope in admin
  before_action { authorize [:admin, User] } # completely prevent access to non-admins
  after_action :verify_authorized

  def index
    @users = User.all
    # preload counts of maps and rows per user; default to 0.
    @counts = %i[maps rows].index_with do |relation|
      @users.joins(relation).group(:id).count
        .tap { |h| h.default = 0 }
    end
  end

  def destroy
    @user = authorize [:admin, User.find(params[:id])]
    if @user.destroy
      redirect_to admin_users_url, notice: t("helpers.message.user.destroyed", user: @user.email)
    else
      if @user.errors.empty?
        # The User#has_many :user_roles relation is dependent: :destroy, but can be aborted in UserRole#map_must_have_an_owner
        # Unfortunately, ActiveRecord does not automatically report the errors to the owner of the relation in this case.
        @user.user_roles.each do |role|
          role.errors.each { @user.errors.import(_1) }
        end
      end
      redirect_to admin_users_url, alert: t("common.failed", msg: @user.errors.full_messages.to_sentence)
    end
  end
end
