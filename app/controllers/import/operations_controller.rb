class Import::OperationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_map, only: [:index, :new, :create]
  before_action :set_from, only: [:new, :create]
  before_action :set_operation, only: [:show, :update, :destroy]

  def index
    operations = policy_scope(Import::Operation).merge(@map.import_operations)
    @operations = operations.where(status: :done)
      .or(operations.where(status: :started, configuration: Import::Configuration.recurring))
  end

  def show
    @operation.analysis
  end

  def new
    mapping = @layer.present? ? Import::Mapping.new(layer: @layer) : nil
    configuration = Import::Configuration.new(map: @map, mappings: Array(mapping))
    @operation = authorize Import::Operation.new(configuration: configuration)
  end

  def create
    if @duplicate_from_operation.present? && @duplicate_from_operation.duplicable?
      @operation = @duplicate_from_operation.deep_dup
      authorize @operation

      did_save = @operation.save
    else
      @operation = Import::Operation.new(import_params)
      authorize @operation

      did_save = @operation.save
      @operation.configure_from_source if did_save
    end

    if did_save
      redirect_to @operation
    else
      render "new", status: :unprocessable_entity
    end
  end

  def update
    success = @operation.update(import_params)

    if success && params[:commit].present? # The user actually clicked the import button
      if @operation.validate(:ready_for_import)
        @operation.import(current_user)
        # TODO: we need to redirect (and not just render "show") here because of a race condition in system tests.
        # (The import job is run inline, and the @operation object here is already outdated when we render.)
        # In fact, we should refactor operations/show.html and merge it with operations/_operation.html.erb.
        # This way, we would simply return head :ok here, and let turbo broadcast handle everything.
        redirect_to @operation
      else
        render "show"
      end
    else
      @operation.configure_from_source
      render "show"
    end
  end

  def destroy
    @operation.destroy
    redirect_to new_map_import_operation_path(@operation.configuration.map, layer_id: @operation.configuration.mappings.first.layer)
  end

  private

  def import_params
    params.require(:import_operation)
      .permit(:local_source_file, :remote_source_url,
        configuration_attributes: [:id, :map_id, :source_type, :name, :source_csv_column_separator, :source_text_encoding, :recurrence,
          mappings_attributes: [:id, :_destroy, :layer_id, :reimport_field_id, :source_layer_name, :source_csv_column_separator, :source_text_encoding,
            :geometry_encoding_format, geometry_columns: [], fields_columns: {}]])
  end

  def set_map
    @map = Map.find(params[:map_id])
  end

  def set_from
    @layer = @map.layers.find_by(id: params[:layer_id])
    @duplicate_from_operation = @map.import_operations.find_by(id: params[:duplicate_from_operation_id])
  end

  def set_operation
    @operation = authorize Import::Operation.with_attached_local_source_file.includes(configuration: [map: :layers, mappings: [layer: :fields]], reports: [mapping: :layer]).find(params[:id])
  end
end
