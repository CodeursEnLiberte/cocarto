# Optimize turbo broadcast changes after a bunch of updates on Row objects
#
# BatchRowBroadcast.transaction do
#   ... do stuff on rows
# end
#
# At the end of the transaction, BatchRowBroadcast automatically broadcasts relevant changes to the frontends
# for rows affected by the transaction (created or updated in an Import::Operation).
# BatchRowBroadcast minimize the number of turbo broadcast and avoids N+1 queries.
class BatchRowBroadcast
  # High-level API
  def self.transaction(emitter: nil, &)
    analyser = Analyser.new
    emitter ||= Emitter.new

    analyser.perform_transaction(&)

    # When rows are touched, we broacast each row individually
    if analyser.rows_touched?
      analyser.changesets.each do |changeset|
        emitter.broadcast_changes(changeset.records.first.layer, *changeset.to_a)
      end
    else # otherwise we are in bulk_mode and broadcast the whole layer
      analyser.layers.each do |layer|
        layer.broadcast_i18n_replace_to layer.map, locals: {initially_active: true}
      end
    end
  end

  # Sort the records changed during a transaction into created, updated and destroyed.
  class Changeset < Struct.new(:records, :created, :updated, :destroyed)
    def self.with_records(records)
      new(
        records: records.to_set,
        created: records.filter(&:previously_new_record?).to_set,
        updated: records.filter { !_1.previously_new_record? && !_1.destroyed? }.to_set,
        destroyed: records.filter(&:destroyed?).to_set
      )
    end

    def to_a
      [created, updated, destroyed]
    end
  end

  # Step 1: Analyser, run the transaction and find which changes need to be notified
  class Analyser
    def perform_transaction(&)
      @records = []

      ApplicationRecord.transaction do
        yield
        @records = ApplicationRecord.connection.current_transaction.records
      end
    end

    def changesets
      @records
        .filter { _1.is_a?(Row) }
        .group_by(&:layer)
        .map { |_, rows| Changeset.with_records(rows) }
    end

    def rows_touched?
      @records.any? { _1.is_a?(Row) }
    end

    def layers
      @records.filter { _1.is_a?(Layer) }
    end
  end

  # Step 2: Emitter, send the actual ActionCable / Turbo Stream notifications to the frontend
  class Emitter
    def broadcast_changes(layer, created_rows, updated_rows, destroyed_rows)
      map = layer.map

      if created_rows.present?
        broadcast_new_rows(map, layer, created_rows)
      end

      if updated_rows.present?
        broadcast_updated_rows(map, layer, updated_rows)
      end

      if destroyed_rows.present?
        broadcast_destroyed_rows(map, destroyed_rows)
      end

      if created_rows.present? || destroyed_rows.present? || changed_map_properties?(layer, updated_rows)
        broadcast_map_layer_update(map, layer)
      end
    end

    def changed_map_properties?(layer, rows)
      rows.any? do |row|
        row.previous_changes.key?(layer.geometry_type) || row.previous_changes.key?(:style)
      end
    end

    private

    def broadcast_new_rows(map, layer, new_rows)
      Row.where(id: new_rows)
        .with_fields_values(layer)
        .includes(layer: :fields)
        .in_batches(of: 1000) do |batch|
        I18n.each_available_locales do |locale|
          # We’re using cache: true here even if the cache will always miss: the result is saved in the cache
          new_html_fragments = ApplicationController.render partial: "rows/row_tr", collection: batch, as: :row, cached: true, locals: {extra_class: "layer-table__tr--transition layer-table__tr--created"}
          turbo_append_tag = ActionController::Base.helpers.turbo_stream_action_tag(:append, target: ActionView::RecordIdentifier.dom_id(layer, :rows), template: new_html_fragments)
          Turbo::StreamsChannel.broadcast_stream_to([map] + [locale], content: turbo_append_tag)
        end
      end
    end

    def broadcast_updated_rows(map, layer, updated_rows)
      Row.where(id: updated_rows)
        .with_fields_values(layer)
        .includes(layer: :fields)
        .in_batches(of: 1000) do |batch|
        I18n.each_available_locales do |locale|
          turbo_replace_tags = batch.map do |row|
            # Can’t use collection rendering here: each fragment has to be in its own turbo tag.
            # (render collection: has a layout: option that we could use to put the turbo tag, but it would save the turbo tag to the cache.)
            html_fragment = ApplicationController.render(RowComponent.new(row: row), layout: false)
            ActionController::Base.helpers.turbo_stream_action_tag(:replace, target: row, template: html_fragment)
          end

          Turbo::StreamsChannel.broadcast_stream_to([map] + [locale], content: turbo_replace_tags.join)
        end
      end
    end

    def broadcast_destroyed_rows(map, destroyed_rows)
      turbo_remove_tags = destroyed_rows.map do |row|
        ActionController::Base.helpers.turbo_stream_action_tag(:remove, target: row)
      end
      Turbo::StreamsChannel.broadcast_stream_to(map, content: turbo_remove_tags.join)
    end

    def broadcast_map_layer_update(map, layer)
      MapUpdateChannel.broadcast_to(map, layer_id: ActionView::RecordIdentifier.dom_id(layer))
    end
  end
end
