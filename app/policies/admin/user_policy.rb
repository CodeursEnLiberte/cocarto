class Admin::UserPolicy < ApplicationPolicy
  def index? = user.admin?

  def destroy? = user.admin?
end
