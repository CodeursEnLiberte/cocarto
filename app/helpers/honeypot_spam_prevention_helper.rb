# honeypot-based spam prevention, inspired from https://github.com/curtis/honeypot-captcha
module HoneypotSpamPreventionHelper
  # Add an invisible input to a form; it should be left empty.
  def honeypot_spam_prevention_tag(name: :a_comment, label: t("common.do_not_fill_in_this_field"))
    html_id = "#{name}_#{SecureRandom.alphanumeric(10)}"
    css = "[id='#{html_id}'] { display:none; }".html_safe # rubocop:disable Rails/OutputSafety
    style_tag = content_tag(:style, css, media: "screen", scoped: "scoped")
    content_tag :div, {id: html_id} do
      style_tag + label_tag(name, label) + text_field_tag(name, "", placeholder: label)
    end
  end

  # Stop processing if the params include the honeypot.
  def honeypot_prevent_spam(controller, context_name = nil, spam_tag_name: :a_comment, only: :create)
    controller.prepend_before_action only: only do
      if params[spam_tag_name].present?
        redirect_to root_path, status: :see_other
        Sentry.set_context(context_name, params[context_name].to_unsafe_h) if params[context_name].present?
        Sentry.capture_message("Blocked a spam signup")
      end
    end
  end
end
