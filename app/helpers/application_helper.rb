module ApplicationHelper
  def current_user_identifier
    current_user.anonymous? ? current_user.anonymous_tag : current_user.id
  end

  # Embeds the svg file directly in the dom
  # This allows to use `currentColor` attribute and style the file with CSS
  # Currently accepted options that are passed to the <svg> tag:
  # - class
  # - style
  def embedded_svg(filename, **options)
    Rails.cache.fetch(["svg", filename, options]) {
      file = Rails.root.join("app", "assets", "images", filename).read
      doc = Nokogiri::HTML::DocumentFragment.parse file
      svg = doc.at_css "svg"
      svg["class"] = options[:class] if options[:class].present?
      svg["style"] = options[:style] if options[:style].present?

      doc.to_html.html_safe # rubocop:disable Rails/OutputSafety
    }
  end

  def link_with_icon text, path, filename, **options
    link_to path, options do
      embedded_svg(filename).concat(text)
    end
  end

  def anchor_link(name, html_options = nil, &)
    anchor = name.parameterize
    link_to(name, "##{anchor}", {id: anchor}.merge(html_options || {}), &)
  end

  def i18n_key_value_pair(field, value)
    value = auto_link(value.to_s, html: {target: "_blank", rel: "noopener"})
    t("common.i18n_key_value_pair", field: field.display_label, value: value.presence || t("common.no_value")).html_safe
  end

  def map_updated_at_span(record, author = nil)
    return if record.nil?

    text = if author.present?
      t("common.updated_at_by", date: time_ago_in_words(record.updated_at), user: author.display_name)
    else
      t("common.updated_at", date: time_ago_in_words(record.updated_at))
    end
    tag.span text, title: l(record.updated_at, format: :long)
  end

  def mailto_contact(name = nil, html_options = nil, &)
    mail_to(t("common.app.contact_email"), name, html_options, &)
  end
end
