class CocartoDeviseMailer < Devise::Mailer
  def invitation_instructions(record, token, opts = {})
    opts[:reply_to] = record.invited_by&.email
    super
  end
end
