class UserRolesMailer < ApplicationMailer
  def invite
    @inviter = params[:inviter]
    @user_role = params[:user_role]
    mail(to: @user_role.user.email, subject: t(".send_invitation", map: @user_role.map.name), reply_to: @inviter.email)
  end
end
