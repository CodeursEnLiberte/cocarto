ActiveSupport.on_load :action_controller do
  next unless self == ActionController::Base

  # Add ViewComponent templates (*_component.html.erb) to the PathRegistry for cache expiry.
  #
  # ViewComponents, by default, don’t play well with rails fragment caching.
  # We rely on another gem, ViewComponent::FragmentCaching, to do it. https://github.com/patrickarnett/view_component-fragment_caching
  # This gem adds the view components view files to the digest used for caching, as well as the components ruby files.
  # This works mostly well, but there seems to be a bug in development mode:
  # changes to the components view files are not taken into account without restarting the server.
  # On the other hand, changes to the components source files are handled correctly.
  # This is because in ViewComponent::FragmentCaching#prepend_view_component_paths, the gem adds a specific “Resolvers::ViewComponentResolver” to the paths.
  # In ActionView::PathRegistry#cast_file_system_resolvers, this resolver is simply added, and a new FileSystemResolver is not needed.
  # This means that the file watcher that normally clears the cache when needed is not rebuilt, in ActionView::CacheExpiry::ViewReloader#rebuild_watcher.
  #
  # All of this is only needed in Rails.env.development?, as the hot-reloading of view with filesystem changes doesn’t make much sense in production.
  #
  # This bug was probaly introduced by this change in ActionView 7.1 https://github.com/rails/rails/commit/8a9976000fc008fa86c1eb196536874388015a15
  # Upstream issue https://github.com/patrickarnett/view_component-fragment_caching/issues/6
  ActionController::Base.prepend_view_path ::ViewComponent::Base.view_component_path
end
