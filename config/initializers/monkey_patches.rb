# Require all Ruby files in the core_extensions directory
Rails.root.glob("lib/core_extensions/*.rb").each { |f| require f }
Rails.root.glob("lib/monkey_patches/*.rb").each { |f| require f }

I18n.extend CoreExtensions::I18nEachAvailableLocale
ActionView::Template.prepend MonkeyPatches::ActionViewTemplateStrictLocalsWithRequestId
