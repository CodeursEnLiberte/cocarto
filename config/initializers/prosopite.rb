unless Rails.env.production?
  # Detect N+1 requests automatically in requests made to controllers
  require "prosopite/middleware/rack"
  Rails.configuration.middleware.use(Prosopite::Middleware::Rack)

  # TODO: insert N+1 detection in GoodJob jobs
end
