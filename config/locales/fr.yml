---
fr:
  api:
    bad_key: Mauvaise clé d’API
  application:
    footer:
      crowdsourcing: Crowdsourcing
      legal_notice: Mentions légales
      presentation: Présentation
      pricing: Tarifs
      privacy: Confidentialité
      resources: Ressources
      source_code: Code source
      terms_of_use: Conditions générales d’utilisation
      use_cases: Cas d’usage
  area:
    million: km²
    unit: m²
  colors:
    blue: Bleu
    brown: Marron
    cyan: Cyan
    green: Vert
    indigo: Indigo
    orange: Orange
    pink: Rose
    purple: Violet
  common:
    add_geometry:
      line_string: Ajouter un tracé
      point: Ajouter un point
      polygon: Ajouter un polygone
      territory: Ajouter un territoire
    app:
      contact_email: bonjour@cocarto.com
      title: cocarto
    area: Surface
    back: Retour
    close: Fermer
    confirm: Êtes-vous sûr ?
    copy: Copier
    created_at: Créé %{date}
    default_map_style: Style de carte par défaut
    do_not_fill_in_this_field: Ne pas remplir ce champ.
    dragbar_cursor: Curseur de la barre de défilement
    edit: Modifier…
    failed: 'Échec : %{msg}'
    i18n_key_value_pair: "%{field} : %{value}"
    latitude: Latitude
    length: Longueur
    longitude: Longitude
    no_value: N/D
    ok: OK
    save: Sauvegarder
    settings: Réglages
    start: Démarrer
    territory_code: Code
    updated_at: Modifié il y a %{date}
    updated_at_by: Modifié il y a %{date} par %{user}
    user_not_authorized: Vous n’avez pas la permission d’effectuer cette action.
  field:
    attach_file: Choisir des fichiers
    attachments: Pièces jointes
    file_added: À envoyer
    file_complete: Terminé
    file_removed: À supprimer
    file_too_big: Nous ne permettons pas les pièces jointes de plus de %{max_size}. Contactez-nous au besoin !
    take_picture: Prendre une photo
  fields:
    form:
      order_asc: croissant
      order_by: Trier par ordre
      order_desc: décroissant
  import:
    errors:
      csv_as_spreadsheet: Le fichier semble être un fichier CSV et non pas un tableur.
    operations:
      form:
        configuration:
          import_as: Importer comme
          recurrence_placeholder: every day at 1
          source_type_help: |-
            Ce sera le type de format utilisé pour interpréter la source de données.
             Il est défini automatiquement par cocarto mais vous pouvez en spécifier un autre.
        mapping_analysis:
          geometry_type: Type de géométrie
          non_geo_data: Données non géographiques
          source: Source
          source_layer_name:
            spreadsheet: Feuille
            wfs: Nom de la couche (“Feature Type”)
        mapping_columns:
          blank: " — ignorer — "
          columns: Correspondance des attributs
        mapping_destination:
          blank_reimport_field: " — ignorer — "
          destination: Destination
          geometry_type: Type de géométrie
          key_field: ID de réimport
          key_field_info: |-
            Pour mettre à jour des données, spécifiez une colonne servant à identifier le rang préexistant.
            Les attributs de chaque rang seront remplacés par ceux présents dans le fichier importé; les attributs absents du fichier ne seront pas supprimés.
            Les nouveaux rangs dans le fichier seront rajoutés au tableau.
          layer: Importer vers
      new:
        file_too_big: Nous ne permettons pas l’import de fichiers de plus de %{max_size}. Contactez-nous au besoin !
        file_too_small: Le fichier à importer ne doit pas être vide.
        help_html: cocarto peut importer des données de fichiers <b>geojson</b>, tabulaires (<b>csv</b>), Excel ou OpenOffice (<b>xls</b>, <b>xlsx</b>, <b>ods</b>), ainsi que depuis un serveur <b>WFS</b>.
        or: ou
        url_placeholder: https://example.com/wfs
      operation:
        duplicate: Refaire le même import
        from_to_html: De <b>%{from}</b> vers <b>%{to}</b>
        imported_bulk_rows: "%{size} rangées importées"
        imported_rows: "%{size} rangées importées sur %{total}"
        imported_rows_details: "%{new} nouvelles, %{updated} mises à jour"
        next_occurrence: 'Prévue : %{next_occurrence}'
        row_failure: Non enregistré.
        to_html: Vers <b>%{to}</b>
  layers:
    form:
      new: Nouvelle couche
    layer:
      center_on_row: Centrer la carte sur cette rangée
      delete_row: Supprimer cette rangée
    layer_header:
      center_on_layer: Centrer la carte sur cette couche
      hide_layer_on_map: Masquer cette couche sur la carte
      show_layer_on_map: Affiche cette couche sur la carte
      toggle_layer: Afficher le tableau de cette couche
  layouts:
    mailer:
      footer_html: <a href="%{root_url}">cocarto</a> est un outil de saisie collaborative de données structurées et géospatialisées. Développé par <a href="https://www.codeursenliberté.fr">codeurs en liberté</a>.
    menu:
      change_language: Changer la langue
      preferences: Préférences
      sign_in: connexion
      sign_out: déconnexion
      sign_up: inscription
  length:
    thousand: km
    unit: m
  line_width_options:
    default: Par défaut
    thick: Épais
    thin: Fin
    very_thick: Très épais
    very_thin: Très fin
  map_tokens:
    map_token:
      access_count:
        one: "%{count} utilisateur anonyme a accédé à ce lien."
        other: "%{count} utilisateurs anonymes ont accédé à ce lien."
        zero: Encore aucun accès enregistré par ce lien.
    new:
      header: Nouveau lien
  maps:
    form:
      first_layer: Première couche
      new: Nouvelle carte
    index:
      create_account: Créer un compte
      role_description:
        contributor: Vous êtes contributeur.
        editor: Vous êtes éditeur.
        owner: Vous êtes propriétaire.
        viewer: Vous avec accès en lecture seule.
    name:
      map_with_no_name: carte cocarto
    sharing:
      header: Partage et permissions
    show:
      all_maps: Cartes
      map: Carte
      share: Partager
      table: Tableau
    toolbar:
      export_as_image: Exporter en tant qu’image
      set_center_and_zoom: Définir centre et zoom de la carte
  pages:
    common:
      action_go_to_map: Afficher la carte
    presentation:
      action_email: Contact
      action_gitlab: Code
      action_mastodon: Mastodon
      action_newsletter: Infolettre
      pricing:
        on_cocarto_com: 'Hébergement de cartes sur cocarto.com :'
        on_premise_html: |
          <p>
            Quotas illimités.
            <a href="mailto:%{contact_email}?subject=Demande%20aide%20au%20déploiement">Demandez-nous une prestation</a> pour vous aider !
          </p>
        on_premise_title: 'Installation sur votre propre serveur :'
        tier_1:
          features_html: |
            <ul>
              <li><b>10</b> cartes maximum</li>
              <li><b>1 000</b> entrées max. par carte</li>
              <li><b>1 Go</b> de pièces jointes</li>
            </ul>
          price: Gratuit
          title: Personnel
        tier_2:
          features_html: |
            <ul>
              <li>Cartes : <b>illimitées</b></li>
              <li>Pièces jointes : <b>illimitées</b></li>
              <li><b>Import WFS</b></li>
              <li><b>Support client</b></li>
            </ul>
          previous_plan: Tout le plan Personnel, plus…
          price: 99 € / mois
          title: Standard
        tier_3:
          features_html: |
            <ul>
              <li><b>Développement</b> de fonctionnalités</li>
              <li><b>Assistance</b> au déploiement</li>
              <li>Support <b>prioritaire</b></li>
              <li><b>Disponibilité</b> garantie</li>
              <li><b>Maintenance</b> garantie</li>
            </ul>
          mail_subject: Demande de prestation
          previous_plan: Tout le plan Standard, plus…
          price: Sur devis
          title: Professionnel
      section_1_html: |
        <p>
          <b>cocarto</b> est un outil de saisie <b>collaborative</b> de données <b>structurées</b> et <b>géospatialisées</b>.
        </p>
        Par exemple :
        <ul>
          <li>le parcours et les étapes d’une randonnée en Corse,</li>
          <li>le taux d’équipement automobile par département en 2019,</li>
          <li>les infrastructures cyclables dans le 19e à Paris.</li>
        </ul>
      section_2_header: Accès facile
      section_2_html: |
        <span>Pas besoin d’être spécialiste pour créer une base de données <b>cohérente</b> et <b>réutilisable</b> :</span>
        <ul>
          <li>Une carte est composée de plusieurs couches d’objets géographiques homogènes : des points, tracés, polygones, territoires administratifs.</li>
          <li>Chaque couche définit des attributs typés : nombres, texte, dates, liste, booléens, images…</li>
        </ul>
        <p>
          cocarto garantit la <b>validité des données</b> saisies. Il est impossible de mettre du texte pour un attribut booléen ou un nombre.<br>
          Les territoires administratifs sont référencés : pas d’ambigüité sur le code postal ou INSEE, ou sur l’orthographe.
        </p>
      section_3_header: Travailler ensemble
      section_3_html: |
        <p>
          <b>cocarto</b> est pensé pour le travail d’équipe.
        </p>
        <p>
          Quand vous travaillez à plusieurs sur la même carte, les contributions des uns et des autres apparaissent en <b>temps réel</b>.<br>
          Vous contrôlez de façon précise qui peut accéder à vos données : en lecture seule, en écriture, ou en contribution ponctuelle…
        </p>
        À venir :
        <ul>
          <li>gestion de l’historique des modifications et retour en arrière ;</li>
          <li>import, export, réimport…</li>
          <li>et <a href="https://gitlab.com/CodeursEnLiberte/cocarto/-/issues" target="_blank">beaucoup d’autres choses</a>.</li>
        </ul>
      section_4_header: Les cartographes vont aimer
      section_4_html: |
        <p>
          <b>cocarto</b> est un logiciel <b>libre</b> et <b>open-source</b>.
          Vous pouvez travailler directement sur <a href="https://cocarto.com">cocarto.com</a>, ou bien installer le logiciel sur vos propres serveurs.
        </p>
        <p>
          cocarto est construit de façon <b>transparente</b> par <a href="https://www.codeursenliberté.fr" target="_blank">Codeurs en Liberté</a>, notre petite société coopérative. Nous ne serons pas rachetés par un géant de la tech.
          Vous pouvez suivre le développement <a href="https://gitlab.com/codeursenliberte/cocarto" target="_blank">sur la page du projet</a> ou via <a href="https://buttondown.email/cocarto" target="_blank">notre infolettre</a>.
        </p>
        <p>
          L’idée de cocarto nous est venue au contact de structures et d’administrations qui partagent des données, de façon plus ou moins artisanale et souvent inconfortable.
          Nous sommes à l’écoute des remarques !<br>
        </p>
        <p>
          <b><a href="mailto:bonjour@cocarto.com">Contactez-nous</a> pour nous faire part de vos idées.</b>
        </p>
      section_pricing_header: Combien ça coûte ?
      section_pricing_html: |
        <p>
          cocarto est un logiciel libre. L’utilisation du plan Personnel sur cocarto.com est <b>gratuite</b>.
          Installer et utiliser cocarto sur votre propre serveur est gratuit aussi.
        </p>
        <p>
          Les cartes hébergées sur cocarto.com comportent toutefois quelques restrictions sur la taille des ressources
          (comme le volume de pièces-jointes).
        </p>
        <p>
          Vous souhaitez des <b>fonctionnalités spécifiques</b>, du <b>support client</b> ou une <b>garantie de maintenance</b> ?
          <a href="mailto:%{contact_email}?subject=Demande%20de%20prestation">Contactez-nous</a> : votre financement aidera cocarto à se développer.
        </p>
    use_case_crowdsourcing:
      banner_html: <a href="///" class="quiet">cocarto</a> est l’outil idéal pour la production participative des données géolocalisées.
      closing_banner: Essayez cocarto
      create_new_map: Créez une carte gratuitement
      hero: Crowdsourcez des données géolocalisées
      section_1_html: "<p>Relever les parkings propices à l’installation de panneaux solaires ?<br>Signaler les nids de poule ?<br>Maintenir le balisage des chemins de randonnée ?</p><p><b>Facile, en s’appuyant sur une équipe de contributeurs.</b></p>"
      section_2_header: Collaborez en temps réel
      section_2_html: "<p>Connectez-vous à plusieurs à la même carte, et travaillez sur vos données : les ajouts et modifications de vos collègues apparaissent en temps-réel.</p>"
      section_3_header: Ajoutez des données sur le terrain
      section_3_html: "<p>Partagez le lien vers le formulaire d’ajout mobile. Maintenant, en ouvrant simplement le lien, vos contributeurs et contributrices peuvent ajouter des données directement.</p><p>Le formulaire mobile permet seulement d’ajouter des éléments. Vous pourrez ensuite relire et valider les ajouts.</p>"
      section_4_header: Partagez à une équipe de contribution
      section_4_html: "<p>Invitez des personnes externes par email. Elles pourront soit modifier les données (Éditeur), soit simplement en ajouter (Contributeur).</p><p>Pour réunir une plus grande équipe, générez un lien d’invitation, et diffusez-le aussi largement que possible.</p>"
      section_5_header: Assurez la qualité de vos données
      section_5_html: "<p>Pour faciliter la saisie, des types vous aident à contraindre les valeurs de chaque champ. Impossible alors de rentrer une valeur fantaisiste ou un code INSEE invalide.</p><p>Et avec le verrouillage de colonnes, protégez une ou plusieurs colonnes des modifications inattendues.</p>"
      section_7_header: Ré-exportez au format de votre choix
      section_7_html: Un simple lien web, et voilà l’export des dernières mises à jour en CSV ou geojson. Pratique pour ré-intégrer une version toujours à jour dans un autre outil.
      section_8_header: Ajoutez des points
      section_8_html: "<p>Alimentez vos données géographiques directement depuis la carte : ajoutez des points, des lignes ou des polygones.</p><p>cocarto est pensé pour la facilité d’utilisation, même par des non-spécialistes.</p>"
      section_9_header: Suivez l’évolution des données
      section_9_html: "<p>Mettez à jour vos données depuis l'interface tableur.</p><p>Triez, modifiez, rajoutez des colonnes pour des informations supplémentaires.</p>"
      try_cocarto: Essayez cocarto
  presence_indicator:
    connected: Connecté
    disconnected: Déconnecté
  rows:
    form:
      color: 'Couleur :'
      layer_radio_color: Couleur par défaut de la couche
      line_width: 'Épaisseur de la ligne :'
      row_radio_color: Couleur spécifique
  territories:
    search_results:
      no_result: Pas de résultat
  time:
    formats:
      media_duration: "%M:%s"
      sentence: le %d %B %Y à %H:%M
  user_roles:
    new:
      header: Nouvel utilisateur
    user_role:
      confirm_delete_self: Êtes-vous sûr ? Vous ne pourrez plus accéder à la carte.
      confirm_relegate_self: Êtes-vous sûr ? Vous ne pourrez plus modifier les permissions.
      invitation_accepted_at: Invitation acceptée %{at}
      invitation_sent_at: 'Invitation envoyée %{at} '
  user_roles_mailer:
    invite:
      access_map: Accéder à la carte
      hello: Bonjour
      invitation_map_html: "%{inviter} vous invite à rejoindre une nouvelle carte : <b>%{map}</b>."
      send_invitation: 'Vous êtes invités à rejoindre la nouvelle carte : %{map}'
  users:
    anonymous: anonyme
