# Pin npm packages by running ./bin/importmap

pin "application", preload: true
pin "process" # @0.11.10
pin "@hotwired/stimulus", to: "@hotwired--stimulus.js" # @3.2.2
pin "@hotwired/turbo-rails", to: "@hotwired--turbo-rails.js" # @8.0.4
pin "@hotwired/turbo", to: "@hotwired--turbo.js" # @8.0.4
pin "@rails/actioncable/src", to: "@rails--actioncable--src.js" # @7.1.3
pin "@rails/activestorage", to: "@rails--activestorage.js" # @7.1.3
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true
pin "maplibre-gl" # @4.3.2
pin_all_from "app/javascript/controllers", under: "controllers"
pin_all_from "app/javascript/channels", under: "channels"
pin_all_from "app/javascript/lib", under: "lib"
pin_all_from "app/javascript/polyfills", under: "polyfills"
pin "@mapbox/mapbox-gl-draw", to: "@mapbox--mapbox-gl-draw.js" # @1.4.3
pin "@floating-ui/dom", to: "@floating-ui--dom.js" # @1.6.5
pin "hotkeys-js" # @3.13.7
pin "@maplibre/maplibre-gl-geocoder", to: "@maplibre--maplibre-gl-geocoder.js" # @1.5.0
pin "events" # @3.3.0
pin "fuzzy" # @0.1.3
pin "lodash.debounce" # @4.0.8
pin "subtag" # @0.5.0
pin "suggestions-list" # @0.0.2
pin "xtend" # @4.0.2
pin "stimulus-use" # @0.52.2
pin "@rails/actioncable", to: "@rails--actioncable.js" # @7.1.3
pin "@floating-ui/core", to: "@floating-ui--core.js" # @1.6.2
pin "@floating-ui/utils", to: "@floating-ui--utils.js" # @0.2.2
pin "@floating-ui/utils/dom", to: "@floating-ui--utils--dom.js" # @0.2.2
