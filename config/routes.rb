Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", :as => :rails_health_check

  scope "(:locale)", locale: Regexp.union(I18n.available_locales.map(&:to_s)) do
    devise_for :users, skip: :invitations, controllers: {registrations: "users/registrations", sessions: "users/sessions"}

    devise_scope :user do
      # Just the wanted subset of invitable routes
      get "/users/invitation/accept" => "devise/invitations#edit", :as => :accept_user_invitation
      put "/users/invitation" => "devise/invitations#update", :as => :user_invitation
    end

    unauthenticated do
      root "pages#presentation"
    end

    authenticated :user do
      root "maps#index", as: :user_root
    end

    resources :maps do
      resources :layers, only: [:new]
      resources :user_roles, only: [:index, :create, :update, :destroy], shallow: true
      resources :map_tokens, only: [:index, :create, :update, :destroy], shallow: true
      namespace :import do
        resources :operations, only: [:index, :new, :create, :show, :update, :destroy], shallow: true, namespace: :import
      end
    end
    resources :layers, except: [:index, :new] do
      resources :fields, only: [:new, :create, :edit, :update, :destroy], shallow: true
      resources :rows, only: [:new, :create, :show, :edit, :update, :destroy], shallow: true
      member do
        get "/mvt/:z/:x/:y/", action: :mvt
      end
    end

    resources :territory_categories, only: [:index, :show]
    resources :territories, only: [:show] do
      collection do
        get "search"
      end
    end

    get "/legal" => "pages#legal"
    get "/legal/conditions" => "pages#legal_conditions"
    get "/legal/data" => "pages#legal_data"
    get "/presentation" => "pages#presentation"
    get "/crowdsourcing" => "pages#use_case_crowdsourcing"
    get "share/:token", to: "maps#shared", as: "map_shared"
    get "layers/:id/geojson", to: redirect("layers/%{id}.geojson")
    get ".well-known/change-password", to: redirect("/users/edit")

    # URLs specific to the instance on cocarto.com
    get "/s/infolettre", to: redirect("https://buttondown.email/cocarto")
    get "/s/geodatadays", to: redirect("https://cocarto.com/share/dg4V524mFETB5Zoo")
    get "/s/commune_de_paris", to: redirect("https://cocarto.com/share/kxw8CL5tD42Do26z"), as: "share_commune_de_paris"
    get "/s/manifs_retraites", to: redirect("https://cocarto.com/share/xuzDBpYpv7jyH_aC"), as: "share_manifs_retraites"
    get "/s/gr20", to: redirect("https://cocarto.com/fr/share/f7iw5cSrm3sdCKRb"), as: "share_gr20"
    get "/s/ymca", to: redirect("https://cocarto.com/fr/share/TQxqzWYz9VYVBpgy")
    get "/s/ymca_m", to: redirect("https://cocarto.com/fr/layers/b00b15eb-c525-40ec-9e2a-1ea0ebf9330e/rows/new?token=TQxqzWYz9VYVBpgy")

    namespace :admin do
      resources :users, only: [:index, :destroy]
    end
  end

  if Rails.env.development?
    mount LetterOpenerWeb::Engine, at: "/letter_opener"
  end

  if Rails.env.development?
    mount GoodJob::Engine, at: "good_job"
    mount ActiveAnalytics::Engine, at: "analytics"
  else
    authenticate :user, ->(user) { user.admin? } do
      mount GoodJob::Engine, at: "good_job"
      mount ActiveAnalytics::Engine, at: "analytics"
    end
  end
end
