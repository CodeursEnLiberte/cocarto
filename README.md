# cocarto

## Context

***Collaborative and historized edition of geographical data***

**cocarto** is a tool for managing geographical-related data that doesn’t require a geomatic engineer experience.  It is a web application that lets you create, edit data sets that include geographical data; multiple users can collaborate in realtime on the same data.

It’s just tabular data with an extra column for the geometry; a spreadsheet of geographical data.

It is *not*:
* a rendering tool to make nice printable maps
* a tool for managing very large datasets.

### How to contribute

This software is distributed under the AGPLv3.

If you want to report a bug, ask for an improvement or submit a fix, open an issue, a merge request or email us at bonjour@codeursenliberte.fr

## Installation for development

### Technical dependencies

**cocarto** is a [Ruby on rails](https://rubyonrails.org/) monolith application. We use [turbo](https://turbo.hotwired.dev/) for interactivity and [stimulus](https://stimulus.hotwired.dev/) for the small bits that need javascript.

In order to work on it, you will need:
- postgresql with postgis
- redis running locally
- [rbenv](https://github.com/rbenv/rbenv-installer#rbenv-installer--doctor-scripts)

### Create the database roles

By default, the app will try to connect to the database with the user `cocarto` and the password `cocarto`.

To create that user, run:

    make setup-pg-users

Those values can also be modified in `config/database.yml`.

### Initialize the development environment

On a new installation, run

    make setup

That command will install the project ruby version, the gem `bundler`, the dependencies and will initialize the database. The database will contain two users:

* `elisee.reclus@commune.paris`, password: `refleurir`
* `cassini@carto.gouv.fr`, password: `générations12345`

## Run the app

### Start the server

To run the server, run:

    make dev

That command will automatically run `make install` to install new dependencies and run database migrations. The app then runs at `https://localhost:3000`.

### Start the server in production

You might want to set extra variables:
- `REDIS_URL` for the cache (otherwise it will fallback to the default cache)
- `COCARTO_S3_*` for row attachments and import from files; see below for S3 CORS configuration.
- `SENTRY_PUBLIC_KEY` to collect production errors

If you use the Procfile, there is nothing special to do.

If you don’t use the Procfile, prepare the assets with

`bundle exec rake assets:precompile RAILS_ENV=production`

### Run the tests

    make test

### Linting

    make lint

We follow the conventions from [StandardRB](https://github.com/testdouble/standard) and [StandardJS](https://standardjs.com/).

## Importing

### Territories

Territories are reference geometries (like countries of the world) that are available to all users.
The territories are grouped into TerritoryCategories that have each a revision to handle evolution over time.

A `rake` task allows to import them, either from an url, either from a local file:

    rake import:geojson[regions.json,Régions de France,2022]

    rake import:geojson[http://etalab-datasets.geo.data.gouv.fr/contours-administratifs/2022/geojson/departements-100m.geojson,Départements de France,2022]

Expect a processing time of about 1s per Mb of geojson.

If you want to set the parent of a territories, you need to pass as an extra-parameter:
* the name of the parent category (`Régions de France`)
* the code of that parent in the dataset feature properties (`region`)


    rake import:geojson[http://etalab-datasets.geo.data.gouv.fr/contours-administratifs/2022/geojson/departements-100m.geojson,Départements de France,2022,Régions de France,region]

### Mock data

The `import_data` thor task is a fronted to the import machinery. A preset configuration is included in the fixtures to add mock data quickly:

```
Usage:
  thor import_data -c "Random Restaurants" -f test/fixtures/files/random_restaurants.json
```

## Hosting

If you want to host the app by yourself, you will need to set the following environment variables:

* PUBLIC_URL: where is your instance running. This is used by the mailer (password recovery…)
* EMAIL_FROM: adress that is used as `From:` in the emails sent
* SMTP_HOST, SMTP_USERNAME, SMTP_PASSWORD, SMTP_PORT: being able to send emails

### S3 CORS configuration

We’re using scaleway for cocarto.com, but this should be similar for any S3 provider. (This lets the S3 provider know that client browsers on cocarto.com can upload files directly to the S3 provider.)

Install the aws cli:

    > pip3 install awscli
    > pip3 install awscli-plugin-endpoint

Setup local config for the aws cli:

    > cat ~/.aws/config
    [plugins] endpoint = awscli_plugin_endpoint
    [default] region = fr-par s3 =
    endpoint_url = https://s3.fr-par.scw.cloud
    
    > cat ~/.aws/credentials
    [default] aws_access_key_id = <scaleway key id> aws_secret_access_key = <scaleway access key>

Prepare a CORS rules json file:

    > cat cors-rules.json
    {
      "CORSRules": [
        {
          "AllowedOrigins": ["https://cocarto.com"],
          "AllowedHeaders": ["*"],
          "AllowedMethods": ["GET", "HEAD", "POST", "PUT", "DELETE"],
          "MaxAgeSeconds": 3000,
          "ExposeHeaders": ["Etag"]
        }
      ]
    }

Send the CORS rules to the S3 provider: (The `file://` prefix is required.)

    > aws s3api put-bucket-cors --bucket <bucket> --cors-configuration file://cors-rules.json

