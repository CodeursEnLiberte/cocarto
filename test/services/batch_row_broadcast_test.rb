require "test_helper"
require "minitest/mock"

class BatchRowBroadcastTest < ActiveSupport::TestCase
  test "BatchRowBroadcast.transaction broadcasts one changeset per layer" do
    mock_emitter = Minitest::Mock.new
    restaurant_rows = Set[]
    hiking_rows = Set[]

    BatchRowBroadcast.transaction(emitter: mock_emitter) do
      restaurant_rows << layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(0 1)")
      restaurant_rows << layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(1 2)")
      hiking_rows << layers(:hiking_paths).rows.create!(author: users(:cassini), line_string: "LINESTRING(0 0, 1 1)")
      hiking_rows << layers(:hiking_paths).rows.create!(author: users(:cassini), line_string: "LINESTRING(2 2, 3 3)")

      mock_emitter.expect(:broadcast_changes, nil, [layers(:restaurants), restaurant_rows, Set[], Set[]])
      mock_emitter.expect(:broadcast_changes, nil, [layers(:hiking_paths), hiking_rows, Set[], Set[]])
    end

    mock_emitter.verify
  end

  class Analyser < self
    test "put the rows changed during the transaction into a changeset" do
      new_rows = Set[]
      row_to_update = layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(1 3)")
      row_to_destroy = layers(:restaurants).rows.first

      analyser = BatchRowBroadcast::Analyser.new
      analyser.perform_transaction do
        row_to_destroy.destroy!
        row_to_update.update!(point: "POINT(0 3)")
        new_rows << layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(0 1)")
        new_rows << layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(1 2)")
      end

      assert_equal 1, analyser.changesets.count
      assert_equal [new_rows, Set[row_to_update], Set[row_to_destroy]], analyser.changesets[0].to_a
    end

    test "split the changed rows into one changeset per layer" do
      new_restaurant_rows = Set[]
      new_hiking_rows = Set[]

      analyser = BatchRowBroadcast::Analyser.new
      analyser.perform_transaction do
        new_restaurant_rows << layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(0 1)")
        new_restaurant_rows << layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(1 2)")
        new_hiking_rows << layers(:hiking_paths).rows.create!(author: users(:cassini), line_string: "LINESTRING(0 0, 1 1)")
        new_hiking_rows << layers(:hiking_paths).rows.create!(author: users(:cassini), line_string: "LINESTRING(2 2, 3 3)")
      end

      assert_equal 2, analyser.changesets.count
      assert_equal [new_restaurant_rows, Set[], Set[]], analyser.changesets[0].to_a
      assert_equal [new_hiking_rows, Set[], Set[]], analyser.changesets[1].to_a
    end
  end

  # Test that Emitter sends the expected number of messages, containing the expected number of turbo actions.
  class Emitter < self
    setup do
      # Add another row to the restaurants layer so that tests make more sens
      layers(:restaurants).rows.create!(author: users(:cassini), point: "POINT(0.0001 0.0001)")
    end

    test "broadcasting new rows" do
      mock_cable = Minitest::Mock.new
      # broadcasts twice for I18nEachAvailableLocale
      mock_cable.expect(:broadcast, nil) { |_stream, content| content.scan('turbo-stream action="append"').size == 1 }
      mock_cable.expect(:broadcast, nil) { |_stream, content| content.scan('turbo-stream action="append"').size == 1 }
      mock_cable.expect(:broadcast, nil) { |stream, _content| stream.include?("map_update") }

      ActionCable.stub :server, mock_cable do
        BatchRowBroadcast::Emitter.new
          .broadcast_changes(layers(:restaurants), layers(:restaurants).rows, [], [])
      end

      mock_cable.verify
    end

    test "broadcasting updated rows" do
      mock_cable = Minitest::Mock.new
      # broadcasts twice for I18nEachAvailableLocale
      mock_cable.expect(:broadcast, nil) { |_stream, content| content.scan('turbo-stream action="replace"').size == 2 }
      mock_cable.expect(:broadcast, nil) { |_stream, content| content.scan('turbo-stream action="replace"').size == 2 }
      mock_cable.expect(:broadcast, nil) { |stream, _content| stream.include?("map_update") }

      ActionCable.stub :server, mock_cable do
        BatchRowBroadcast::Emitter.new
          .broadcast_changes(layers(:restaurants), [], layers(:restaurants).rows, [])
      end

      mock_cable.verify
    end

    test "broadcasting deleted rows" do
      mock_cable = Minitest::Mock.new
      mock_cable.expect(:broadcast, nil) { |_stream, content| content.scan('turbo-stream action="remove"').size == 2 }
      mock_cable.expect(:broadcast, nil) { |stream, _content| stream.include?("map_update") }

      ActionCable.stub :server, mock_cable do
        BatchRowBroadcast::Emitter.new
          .broadcast_changes(layers(:restaurants), [], [], layers(:restaurants).rows)
      end

      mock_cable.verify
    end
  end
end
