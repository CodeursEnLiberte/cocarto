require "test_helper"

class Importers::CSVTest < ActiveSupport::TestCase
  class Analyse < self
    class SourceConfiguration < self
      test "commas" do
        csv = <<~CSV
          Name,Rating,Table Size
          L’Antipode,9,70
          Le Bastringue,8,50
        CSV

        analysis = Importers::CSV.new(::Import::Configuration.new, csv, nil)._source_configuration

        assert_equal ",", analysis[:source_csv_column_separator]
      end

      test "semicolons" do
        csv = <<~CSV
          CODREG;REG;CODDEP;DEP;NBARR;NBCAN;NBCOM;PMUN;PTOT
          84;Auvergne-Rhône-Alpes;01;Ain;4;23;393;657856;673801
          32;Hauts-de-France;02;Aisne;5;21;799;529374;541176
        CSV

        analysis = Importers::CSV.new(::Import::Configuration.new, csv, nil)._source_configuration

        assert_equal ";", analysis[:source_csv_column_separator]
      end

      test "mixed" do
        csv = <<~CSV
          code;categorie,numéro;addresse
          ZZ;A23;1 rue bleue, 12345 levillage
          AA;B12;une longue adresse
        CSV

        analysis = Importers::CSV.new(::Import::Configuration.new, csv, nil)._source_configuration

        assert_equal ";", analysis[:source_csv_column_separator]
      end
    end

    test "layer_columns" do
      csv = file_fixture("restaurants.csv").open
      config = maps(:restaurants).import_configurations.new

      source_columns = Importers::CSV.new(config, csv, nil)._source_columns(0)

      assert_equal({"Name" => String, "Rating" => String, "Table Size" => String, "Ville" => String, "Date" => NilClass, "Decision" => NilClass, "geojson" => String}, source_columns)
    end

    test "mapping_geometry_attributes" do # rubocop:disable Minitest/MultipleAssertions
      csv = file_fixture("restaurants.csv").open
      config = maps(:restaurants).import_configurations.new

      geometry_analysis = Importers::CSV.new(config, csv, nil)._source_geometry_analysis(0)

      assert_equal ["geojson"], geometry_analysis.columns
      assert_equal :geojson, geometry_analysis.format
      assert_equal RGEO_FACTORY.point(2.37516, 48.88661), geometry_analysis.geometry
      assert_equal "Point", geometry_analysis.type
    end
  end

  class Import < self
    test "import csv" do
      layers(:restaurants).rows.destroy_all

      csv = file_fixture("restaurants.csv").open
      config, mapping = preconfigured_import(:restaurants, :csv, csv)

      assert_changes -> { layers(:restaurants).rows.count }, from: 0, to: 2 do
        Importers::CSV.new(config, csv, users(:reclus)).import_rows(mapping.reports.new)
      end

      assert_equal territories(:paris), layers(:restaurants).rows.last.fields_values[fields(:restaurant_ville)]
    end
  end

  class Automatic < self
    test "Automatic separator detection" do
      layers(:restaurants).rows.destroy_all

      csv = <<~CSV
        Nom;Convives;geojson
        L’Antipode;70;"{""type"":""Point"",""coordinates"":[2.37516,48.88661]}"
      CSV
      config, mapping = preconfigured_import(:restaurants, :csv, csv)

      assert_changes -> { layers(:restaurants).rows.count }, from: 0, to: 1 do
        Importers::CSV.new(config, csv, users(:reclus)).import_rows(mapping.reports.new)
      end
    end
  end
end
