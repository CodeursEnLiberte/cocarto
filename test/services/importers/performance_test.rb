require "test_helper"

class Importers::PerformanceTest < ActiveSupport::TestCase
  def import_regions(bulk_mode:)
    geojson = file_fixture("regions.geojson").open
    config, mapping = preconfigured_import(:hiking_zones, :geojson, geojson)
    mapping.bulk_mode = bulk_mode
    Importers::GeoJSON.new(config, geojson, users(:reclus)).import_rows(mapping.reports.new)
  end

  test "bulk mode vs regular" do # rubocop:disable Minitest/MultipleAssertions
    realtime_bulk, realtime_regular = 0

    layers(:hiking_zones).rows.destroy_all
    report_regular = MemoryProfiler.report { realtime_regular = Benchmark.realtime { import_regions(bulk_mode: false) } }

    layers(:hiking_zones).rows.destroy_all
    report_bulk = MemoryProfiler.report { realtime_bulk = Benchmark.realtime { import_regions(bulk_mode: true) } }

    assert_operator realtime_bulk, :<, realtime_regular

    # macbook m1, ruby 3.2.2, rails 7.1.2
    #                         |       bulk |    regular
    # total_allocated_memsize | 10_954_658 | 27_024_405
    # total_retained_memsize  |    163_790 |  1_140_593
    assert_operator report_bulk.total_allocated_memsize, :<, report_regular.total_allocated_memsize
    assert_operator report_bulk.total_retained_memsize, :<, report_regular.total_retained_memsize

    assert_operator report_bulk.total_allocated_memsize, :<=, 10_954_658 * 1.1
    assert_operator report_bulk.total_retained_memsize, :<=, 163_790 * 1.1
  end
end
