# On macOS, when the `vips` native library is loaded in a forked process, it may produce the following error:
#
# > +[NSCheapMutableString initialize] may have been in progress in another thread when fork() was called.
# >   We cannot safely call it or ignore it in the fork() child process. Crashing instead.
# >   Set a breakpoint on objc_initializeAfterForkError to debug.
#
# The usual workaround is to `export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES`, which allows the process to continue.
# However this is unsafe, and can lead to deadlocks.
# (See http://sealiesoftware.com/blog/archive/2017/6/5/Objective-C_and_fork_in_macOS_1013.html)
#
# Instead we preload `vips` *before* the test process is forked, thus avoiding the error.
#
# See https://github.com/libvips/ruby-vips/issues/155
if Rails.application.config.active_storage.variant_processor == :vips && RUBY_PLATFORM.include?("darwin")
  require "ruby-vips"
end
