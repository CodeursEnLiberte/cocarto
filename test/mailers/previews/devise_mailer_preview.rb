# Preview all emails at https://localhost:3000/rails/mailers/devise_mailer
class DeviseMailerPreview < ActionMailer::Preview
  def invitation_instructions
    user_fixture = User.first
    user_fixture.invited_by = User.second
    Devise.mailer.invitation_instructions(user_fixture, "__token__")
  end

  def reset_password_instructions
    Devise.mailer.reset_password_instructions(User.first, "__token__")
  end
end
