# == Schema Information
#
# Table name: fields
#
#  id                 :uuid             not null, primary key
#  display_in_tooltip :boolean          default(TRUE), not null
#  enum_values        :string           is an Array
#  field_type         :enum             not null
#  label              :string
#  locked             :boolean          default(FALSE), not null
#  sort_order         :integer
#  text_is_long       :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  layer_id           :uuid             not null
#
# Indexes
#
#  index_fields_on_layer_id_and_sort_order  (layer_id,sort_order) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (layer_id => layers.id)
#
require "test_helper"

class FieldTest < ActiveSupport::TestCase
  class Ordering < self
    test "new field is appended at the end" do
      new_field = layers(:restaurants).fields.type_text.create!

      assert_equal new_field.sort_order_rank, layers(:restaurants).fields.count - 1
    end

    test "reordering a row correctly updates the ranks" do
      field_1 = layers(:restaurants).fields.first
      field_2 = layers(:restaurants).fields.second

      field_1.update(sort_order_position: :down)

      assert_equal 1, field_1.sort_order_rank
      assert_equal 0, field_2.reload.sort_order_rank
    end
  end

  class Enum < self
    test "some enum value is allowed" do
      field = Field.type_enum.new(enum_values: ["value"])
      field.validate

      assert_empty field.errors.details[:enum_values]
    end

    test "several enum values are allowed" do
      field = Field.type_enum.new(enum_values: %w[a b c])
      field.validate

      assert_empty field.errors.details[:enum_values]
    end

    test "no enum value is invalid" do
      field = Field.type_enum.new
      field.validate

      assert_equal [{error: :blank}], field.errors.details[:enum_values]
    end

    test "only blank value is invalid" do
      field = Field.type_enum.new(enum_values: [""])
      field.validate

      assert_equal [{error: :blank}], field.errors.details[:enum_values]
    end

    test "blank enum values are removed" do
      field = Field.type_enum.new(enum_values: ["a", "b", "c", ""])

      assert_equal %w[a b c], field.enum_values
    end

    test "duplicate enum values are removed" do
      field = Field.type_enum.new(enum_values: %w[a b c c])

      assert_equal %w[a b c], field.enum_values
    end
  end

  class Territory < self
    test "only valid territory id" do
      territory_field = layers(:restaurants).fields.type_territory.create!(territory_categories: [territory_categories(:communes)])

      assert_nil territory_field.cast("lol")
      assert_equal territories(:paris).id, territory_field.cast(territories(:paris))
      assert_equal territories(:paris).id, territory_field.cast(territories(:paris).id)
    end

    test "reject territory of incorrect category" do
      territory_field = layers(:restaurants).fields.type_territory.create!(territory_categories: [territory_categories(:communes)])

      assert_nil territory_field.cast(territories(:idf).id)
    end

    test "territory by code" do
      territory_field = layers(:restaurants).fields.type_territory.create!(territory_categories: [territory_categories(:communes)])

      assert_equal territories(:paris).id, territory_field.cast(territories(:paris).code)
    end
  end

  class Types < self
    test "correctly cast strings to booleans" do
      bool_field = Field.type_boolean.new

      assert_includes [true, false], bool_field.cast("1")
      assert_not bool_field.cast("0")
      assert_nil bool_field.cast("")
    end

    test "correctly cast strings to integers" do
      int_field = Field.type_integer.new

      assert_equal 42, int_field.cast("42")
      assert_nil int_field.cast("")
    end

    test "correctly cast strings to floats" do
      float_field = Field.type_float.new

      assert_in_epsilon 3.14, float_field.cast("3.14")
      assert_nil float_field.cast("")
      assert_nil float_field.cast(nil)
    end

    test "don’t cast fields that don’t need to" do
      enum_field = Field.type_enum.new(enum_values: %w[a b c])

      assert_equal "b", enum_field.cast("b")

      text_field = Field.type_text.new

      assert_equal "hello", text_field.cast("hello")
      assert_equal "", text_field.cast("")
    end

    test "correcly cast css-properties" do
      stroke = Field.type_css_property.new(label: "stroke-width")

      assert_equal 3, stroke.cast("3")
      assert_nil stroke.cast("")

      color = Field.type_css_property.new(label: "color")

      assert_equal "333", color.cast("333")
    end

    test "cast file attachments" do
      file_field = Field.type_files.new

      assert_empty file_field.cast(["_empty"])
      assert_empty file_field.cast(["invalid-identifier"])
      assert_equal [active_storage_blobs(:touladi).id], file_field.cast([active_storage_blobs(:touladi).signed_id])
    end
  end

  class SumTest < self
    test "Float sum has correct value and type" do
      assert_in_delta 9.0, fields(:restaurant_rating).sum
      assert_instance_of BigDecimal, fields(:restaurant_rating).sum
    end

    test "Integer sum has correct value and type" do
      assert_equal 70, fields(:restaurant_table_size).sum
      assert_instance_of BigDecimal, fields(:restaurant_table_size).sum
    end

    test "Large value does not crash" do
      # cf https://www.postgresql.org/docs/current/datatype-numeric.html, 2147483647 is the maximum integer value
      rows(:antipode).update(fields_values: {fields(:restaurant_table_size).id => 2147483648})

      assert_nothing_raised do
        assert_equal 2147483648, fields(:restaurant_table_size).sum
      end
    end
  end
end
