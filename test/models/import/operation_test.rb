# == Schema Information
#
# Table name: import_operations
#
#  id                :uuid             not null, primary key
#  global_error      :string
#  remote_source_url :string
#  status            :enum             default("ready"), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  configuration_id  :uuid             not null
#
# Indexes
#
#  index_import_operations_on_configuration_id  (configuration_id)
#
# Foreign Keys
#
#  fk_rails_...  (configuration_id => import_configurations.id)
#
require "test_helper"

class Import::OperationTest < ActiveSupport::TestCase
  class Validation < self
    test "neither local nor remote" do
      operation = Import::Operation.new(configuration: import_configurations(:restaurants_csv))

      assert_not operation.valid?
      assert_equal({remote_source_url: [{error: :blank}]}, operation.errors.details)
    end

    test "local and remote" do
      operation = Import::Operation.new(configuration: import_configurations(:restaurants_csv), local_source_file: attachable_fixture("restaurants.csv"), remote_source_url: "https://example.com/api")

      assert_not operation.valid?
      assert_equal({remote_source_url: [{error: :present}]}, operation.errors.details)
    end
  end

  class DeepDup < self
    setup { start_fixtures_server }

    test "only operation with remote sources are #duplicable?" do
      operation = import_configurations(:restaurants_csv).operations.new(local_source_file: attachable_fixture("restaurants.csv"))

      assert_not_predicate operation, :duplicable?

      operation2 = import_configurations(:hiking_paths_wfs).operations.new(remote_source_url: "https://example.com/api")

      assert_predicate operation2, :duplicable?
    end

    test "#deep_dup copies the configuration and resets the status" do # rubocop:disable Minitest/MultipleAssertions
      operation = import_configurations(:hiking_paths_wfs).operations.create(remote_source_url: "https://example.com/api", status: "done", global_error: "Failure")

      operation2 = operation.deep_dup

      assert_equal "ready", operation2.status
      assert_nil operation2.global_error
      assert_predicate operation2.configuration, :new_record?
      assert_predicate operation2.configuration.mappings.first, :new_record?
    end

    test "#deep_dup keeps the analysis cache" do
      use_memory_store!

      operation = import_configurations(:hiking_paths_wfs).operations.create(remote_source_url: "#{fixtures_server_url}/wfs")

      assert_not_nil operation.analysis

      operation2 = operation.deep_dup
      operation2.configuration = nil

      assert_not_nil operation2.analysis
      assert_not_nil Rails.cache.read(%W[analysis #{fixtures_server_url}/wfs])
    end
  end

  class Hooks < self
    test "source_type is automatically set from the first operation" do
      conf = Import::Configuration.new(map: maps(:restaurants))
      op = Import::Operation.new(local_source_file: attachable_fixture("restaurants.csv"), configuration: conf)
      op.save

      assert_predicate op, :persisted?

      assert_equal "csv", conf.source_type
    end
  end

  class GlobalResult < self
    test "unknown type error" do
      operation = import_configurations(:restaurants_geojson)
        .operations.create!(local_source_file: attachable_data("restaurants.txt", "some data"))
        .import!(users(:reclus))

      assert_not operation.success?
      assert_equal "unexpected character: 'some data' (MultiJson::ParseError)", operation.global_error
    end

    test "geojson parsing error" do
      operation = import_configurations(:restaurants_geojson)
        .operations.create!(local_source_file: attachable_data("restaurants.json", "Ceci n’est pas un geojson."))
        .import!(users(:reclus))

      assert_not operation.success?
      assert_equal "unexpected character: 'Ceci n’est pas un geojson.' (MultiJson::ParseError)", operation.global_error
    end

    test "csv parsing error" do
      operation = import_configurations(:restaurants_csv)
        .operations.create!(local_source_file: attachable_data("restaurants.csv", 'Ceci,""n’est, pas un csv.'))
        .import!(users(:reclus))

      assert_not operation.success?
      assert_equal "Any value after quoted field isn't allowed in line 1. (CSV::MalformedCSVError)", operation.global_error
    end

    test "wrong geometry type" do
      conf = Import::Configuration.create(map: maps(:hiking), source_type: :csv)
      conf.mappings.create(layer: layers(:hiking_paths), geometry_columns: ["geojson"], geometry_encoding_format: :geojson)
      operation = conf.operations.create!(local_source_file: attachable_fixture("restaurants.csv"))
      operation.configure_from_source
      operation.import!(users(:reclus))

      assert_not operation.success?
      assert_equal "PG::InvalidParameterValue: ERROR:  Geometry type (Point) does not match column type (LineString) (ActiveRecord::StatementInvalid)", operation.global_error
    end

    test "wrong sheet name" do
      import_mappings(:restaurants_spreadsheet).update(source_layer_name: "invalid sheet name")
      operation = import_configurations(:restaurants_spreadsheet)
        .operations.create!(local_source_file: attachable_fixture("restaurants.xlsx"))
        .import!(users(:reclus))

      assert_not operation.success?
      assert_equal "sheet 'invalid sheet name' not found (RangeError)", operation.global_error
    end

    test "success" do
      operation = import_configurations(:restaurants_csv)
        .operations.create!(local_source_file: attachable_fixture("restaurants.csv"))
      operation.configure_from_source
      operation.import!(users(:reclus))

      assert_predicate operation, :success?
    end
  end

  class RemoteSourceToLocalFile < self
    setup { start_fixtures_server }

    test "download a remote csv" do
      operation = import_configurations(:restaurants_csv)
        .operations.create!(remote_source_url: "#{fixtures_server_url}/restaurants.csv")
      operation.configure_from_source
      operation.import!(users(:reclus))

      assert_predicate operation, :success?
    end
  end
end
