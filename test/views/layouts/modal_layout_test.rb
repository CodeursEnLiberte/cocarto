require "test_helper"

class ModalLayoutTest < ActionDispatch::IntegrationTest
  ROUTE_PATH = "/test_subclassed_ModalLayoutTest"

  def self.setup_route_once!
    Rails.application.routes.disable_clear_and_finalize = true # preserve original routes
    Rails.application.routes.draw do
      get ROUTE_PATH, controller: "modal_layout_test/test_subclassed", action: :index
    end
  end
  setup_route_once!

  class TestSubclassedController < ApplicationController
    before_action :skip_authorization, :skip_policy_scope
    def index
      render inline: "<h1>Index</h1>", layout: true # rubocop:disable Rails/RenderInline
    end
  end

  test "normal requests use the application layout" do # rubocop:disable Minitest/MultipleAssertions
    get ROUTE_PATH

    assert_response :success
    assert_select "html > head", 1, "A <head> element must be present"
    assert_select "body .header__container", 1, "The app header must be present"
    assert_select "body .footer__container", 1, "The app footer must be present"
  end

  test "turbo-frame requests use a simplified layout" do # rubocop:disable Minitest/MultipleAssertions
    get ROUTE_PATH, headers: {"Turbo-Frame": "some-frame-id"}

    assert_response :success
    assert_select "html > head > *", false, "The head element should be empty"
    assert_select "body .header__container", false, "No app header should be present"
    assert_select "body .footer__container", false, "No app footer should be present"
  end

  test "modal turbo-frame requests use a modal layout" do # rubocop:disable Minitest/MultipleAssertions
    get ROUTE_PATH, headers: {"Turbo-Frame": "modal"}

    assert_response :success
    assert_select "html > head > *", false, "The head element should be empty"
    assert_select "body .header__container", false, "No app header should be present"
    assert_select "body .footer__container", false, "No app footer should be present"
    assert_select "turbo-frame[id=modal]", 1, "A turbo-frame modal container should be present"
  end
end
