require "application_system_test_case"

class MapSharingTest < ApplicationSystemTestCase
  test "the map owner can see the roles" do
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("boat"))
    click_link "Share"

    assert_field "user_role_user_attributes_email", with: "elisee.reclus@commune.paris"
    assert_field "user_role_user_attributes_email", with: "cassini@carto.gouv.fr"
  end

  test "a viewer can see the map, but not the roles" do
    sign_in_as(users("cassini"), "générations12345")
    visit map_path(id: maps("boat"))

    assert_no_link "Share"
  end

  test "inviting an user" do # rubocop:disable Minitest/MultipleAssertions
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("boat"))
    click_link "Share"

    within "#new_user_role" do
      select "Editor", from: "Role"
      fill_in "Email", with: "invited-user@example.com"
      click_on "Invite"
    end

    assert_text "Invitation sent"

    # Disconnect the user
    clear_session

    # Click on the invitation link
    open_email("invited-user@example.com")

    assert_includes current_email.subject, "Invitation"
    current_email.click_link "Accept invitation"

    # Register as a new user
    assert_current_path "/users/invitation/accept", ignore_query: true
    assert_field "Display name", with: "invited-user"
    assert_field "Email", with: "invited-user@example.com", readonly: true
    fill_in "Password", with: "MyPassword!"
    fill_in "Confirm new password", with: "MyPassword!"
    click_on "Set my password"

    # The invited user can access the map
    assert_text "Boating trip"
    click_on "Boating trip"

    assert_current_path map_path(id: maps("boat"), locale: "en")
  end

  test "generate a link for an anonymous access" do
    # Let’s be sure we can’t access the page
    maps("boat").map_tokens.destroy_all
    visit map_path(id: maps("boat"))

    assert_no_field "Name"

    # Create a link
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("boat"))
    click_link "Share"
    click_link "Links"

    new_link = find_by_id("new_map_token")
    new_link.select("Editor", from: "Role")
    new_link.fill_in("Description", with: "Test link")
    click_button "Create link"

    url = find("input", id: "url_to_share").value
    # The boats map has no point layer, so there can’t be an url to contribute
    assert_no_selector("a", id: "url_to_contribute")
    click_button "Close"

    # We sign out and we can access the page
    sign_out
    visit(url)

    assert_selector "h2", text: "Boating trip"
  end

  test "generate contribution form links" do
    maps("restaurants").map_tokens.destroy_all
    visit map_path(id: maps("restaurants"))

    # Create a link
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("restaurants"))
    click_link "Share"
    click_link "Links"

    new_link = find_by_id("new_map_token")
    new_link.select("Editor", from: "Role")
    new_link.fill_in("Description", with: "Test link")
    click_button "Create link"

    url = find("input", id: "url_to_contribute").value

    click_button "Close"
    sign_out
    visit(url)

    assert_selector "h3", text: "Restaurants / Restaurants"
    assert_selector("input", id: "row_fields_values_#{fields(:restaurant_name).id}")
  end

  test "generate view links that doesn’t allow contribution" do
    maps("restaurants").map_tokens.destroy_all
    visit map_path(id: maps("restaurants"))

    # Create a link
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("restaurants"))
    click_link "Share"
    click_link "Links"

    new_link = find_by_id("new_map_token")
    new_link.select("Viewer", from: "Role")
    new_link.fill_in("Description", with: "Test link")
    click_button "Create link"

    assert_no_selector("a", id: "url_to_contribute")
  end

  test "anonymous access" do # rubocop:disable Minitest/MultipleAssertions
    visit map_shared_url(token: map_tokens(:restaurants_contributors).token)
    wait_until_turbo_stream_connected
    wait_until_map_loaded
    visit map_shared_url(token: map_tokens(:boat_viewers).token)
    wait_until_turbo_stream_connected
    wait_until_map_loaded

    # Both maps should be accessible
    click_link "Maps"

    assert_current_path "/en" + maps_path
    assert_text "Restaurants"
    assert_text "Boating trip"

    # Existing row is not editable
    visit map_path(id: maps("restaurants"), open: layers("restaurants").id)
    wait_until_turbo_stream_connected
    map = wait_until_map_loaded

    within("##{dom_id(rows(:antipode))}") do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", disabled: true
    end

    # New row is editable
    click_on "Add a point"
    map.click(x: 0, y: 50)
    row_pavillon = layers(:restaurants).rows.order(:created_at).last

    # Edit in table
    within("##{dom_id(row_pavillon)}") do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]"
      find_field("row[fields_values][#{fields("restaurant_name").id}]")
        .set("Le pavillon des canaux").native.send_keys(:return)
    end

    # Edit in modal
    within("##{dom_id(row_pavillon)}") do
      click_on "Edit…"
    end

    within ".modal" do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]"
      find_field("row[fields_values][#{fields("restaurant_name").id}]")
        .set("Le pavillon des canaux").native.send_keys(:return)
    end

    # Visit as a new anonymous contributor
    clear_session

    visit map_shared_url(token: map_tokens(:restaurants_contributors).token)

    # Neither row is editable
    within("##{dom_id(rows(:antipode))}") do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", disabled: true
    end
    within("##{dom_id(row_pavillon)}") do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", disabled: true
    end
  end

  test "anonymous access then sign in" do
    users(:bakounine).user_roles.destroy_all

    visit map_shared_url(token: map_tokens(:restaurants_contributors).token)
    sign_in_as(users(:bakounine), "refleurir")
    click_link "Restaurants"

    assert_current_path map_path(id: maps("restaurants"), locale: "en")

    sign_out
    visit maps_url

    assert_text "You need to sign in or sign up before continuing."
  end

  test "anonymous access then sign up" do
    visit map_shared_url(token: map_tokens(:restaurants_contributors).token)
    wait_until_dropdown_controller_ready
    click_button "anonymous"
    click_link "Sign up"
    fill_in "user_email", with: "cabiai@amazonas.br"
    fill_in "user_password", with: "canne à sucre"
    fill_in "user_password_confirmation", with: "canne à sucre"
    click_button "Sign up"
    click_link "Restaurants"

    assert_current_path map_path(id: maps("restaurants"), locale: "en")

    sign_out
    visit maps_url

    assert_text "You need to sign in or sign up before continuing."
  end
end
