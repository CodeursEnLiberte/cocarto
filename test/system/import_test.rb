require "application_system_test_case"

class ImportTest < ApplicationSystemTestCase
  class FileImportTest < ImportTest
    test "import from a csv file" do  # rubocop:disable Minitest/MultipleAssertions
      use_memory_store!

      layers("restaurants").rows.destroy_all

      sign_in_as(users("reclus"), "refleurir")
      visit map_path(id: maps("restaurants"), open: layers("restaurants").id)
      wait_until_turbo_stream_connected

      # Import restaurants (without ratings)
      click_on "Import…"
      attach_file("file", file_fixture("restaurants_without_ratings.csv").to_path, make_visible: true)

      assert_select "Name", selected: "Name"
      assert_select "Table Size", selected: "Table Size"
      assert_select "Ville", selected: "Ville"
      assert_select "Date", selected: "Date"
      assert_select "Decision", selected: "Decision"

      click_on "Start"

      assert_text "Import successful"
      assert_text "2 rows imported"

      click_on "OK"

      assert_field "row[fields_values][#{fields("restaurant_rating").id}]", with: "", count: 2
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", with: "Le Bastringue"
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", with: "L’Antipode"

      # Reimport restaurant ratings from a csv with non geo data
      click_on "Import…"
      attach_file("file", file_fixture("restaurants_ratings.csv").to_path, make_visible: true)
      select "Name", from: "Reimport ID"

      assert_select "Name", selected: "Name"
      assert_select "Rating", selected: "Rating"

      click_on "Start"

      assert_text "Import successful"
      assert_text "2 rows imported"

      click_on "OK"

      assert_field "row[fields_values][#{fields("restaurant_rating").id}]", with: "10.0", count: 2
    end

    test "import failure" do  # rubocop:disable Minitest/MultipleAssertions
      sign_in_as(users("reclus"), "refleurir")
      visit map_path(id: maps("restaurants"), open: layers("restaurants").id)
      wait_until_turbo_stream_connected

      click_on "Import…"
      attach_file("file", file_fixture("touladi.png").to_path, make_visible: true)

      assert_text "1 error prohibited this data import from being saved:"
      assert_text "This source type is not supported."
    end

    test "authors during re-import" do
      sign_in_as(users("reclus"), "refleurir")
      visit map_path(id: maps("restaurants"), open: layers("restaurants").id)
      wait_until_turbo_stream_connected

      click_on "Import…"
      attach_file("file", file_fixture("restaurants_ratings.csv").to_path, make_visible: true)
      select "Name", from: "Reimport ID"
      click_on "Start"

      assert_text "Import successful"

      # Make sure that we don’t override the original author or anonymous tag when re-importing
      assert_nil rows("antipode").author
      assert_equal "secret", rows("antipode").anonymous_tag
    end
  end

  class ServerImportTest < ImportTest
    setup { start_fixtures_server }

    test "import from a wfs server" do  # rubocop:disable Minitest/MultipleAssertions
      sign_in_as(users("cassini"), "générations12345")
      visit map_path(id: maps("hiking"), open: layers("hiking_paths").id)
      wait_until_turbo_stream_connected

      click_on "Import…"
      fill_in "Data source URL", with: "#{fixtures_server_url}/wfs"
      click_on "OK"
      scroll_to(find_button("Start", wait: 10), :center) # WFS query may take a while
      click_on "Start"

      assert_text "Import successful", wait: 10 # The import may take a while
      assert_text "4 rows imported out of 4"

      click_on "OK"

      assert_selector "input[value='Tracé numéro un']", count: 1
    end
  end

  class RecurringImportTest < ImportTest
    setup { start_fixtures_server }

    test "Setup a recurring import" do  # rubocop:disable Minitest/MultipleAssertions
      sign_in_as(users("cassini"), "générations12345")
      users("cassini").update!(admin: true) # The recurrence is only for admin at the moment.

      visit map_path(id: maps("hiking"), open: layers("hiking_paths").id)
      wait_until_turbo_stream_connected

      click_on "Import…"
      fill_in "Data source URL", with: "#{fixtures_server_url}/wfs"
      click_on "OK"
      scroll_to(find_button("Start", wait: 10), :center) # WFS query may take a while

      fill_in "Recurrence", with: "Some day"
      click_on "Start"

      assert_text "Configuration recurrence is invalid"

      fill_in "Recurrence", with: "every day at 1:02"
      click_on "Start"

      assert_text "In progress"
      click_on "Close"

      assert_no_text "In progress"
      click_link "Imports"

      assert_text(/Expected at.* 01:02/)
    end
  end
end
