class AddImportReportBulkCount < ActiveRecord::Migration[7.1]
  def change
    add_column :import_reports, :bulk_count, :integer
  end
end
