class AddStyleToMaps < ActiveRecord::Migration[7.1]
  def change
    add_column :maps, :base_style_url, :text
  end
end
