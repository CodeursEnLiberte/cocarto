class MakeDoctorHappy < ActiveRecord::Migration[7.1]
  def change
    add_index :fields_territory_categories, [:field_id, :territory_category_id], unique: true
    add_index :layers_territory_categories, [:layer_id, :territory_category_id], unique: true
    remove_index :fields_territory_categories, :field_id
    remove_index :layers_territory_categories, :layer_id
  end
end
