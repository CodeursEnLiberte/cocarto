class AddNewColumnToFields < ActiveRecord::Migration[7.0]
  def change
    add_column :fields, :display_in_tooltip, :boolean, default: false, null: false
  end
end
