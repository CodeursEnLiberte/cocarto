class AddColumnStyleToRow < ActiveRecord::Migration[7.0]
  def change
    add_column :rows, :style, :jsonb, default: {}, null: false
  end
end
