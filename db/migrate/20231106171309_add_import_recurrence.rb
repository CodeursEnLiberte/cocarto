class AddImportRecurrence < ActiveRecord::Migration[7.0]
  def change
    add_column :import_configurations, :recurrence, :string, default: "", null: false
  end
end
