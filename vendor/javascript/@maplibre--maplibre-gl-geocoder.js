// @maplibre/maplibre-gl-geocoder@1.5.0 downloaded from https://ga.jspm.io/npm:@maplibre/maplibre-gl-geocoder@1.5.0/lib/index.js

import*as e from"suggestions-list";import*as t from"lodash.debounce";import*as i from"xtend";import*as s from"events";import*as o from"subtag";var r={};r={fr:{name:"France",bbox:[[-4.59235,41.380007],[9.560016,51.148506]]},us:{name:"United States",bbox:[[-171.791111,18.91619],[-66.96466,71.357764]]},ru:{name:"Russia",bbox:[[19.66064,41.151416],[190.10042,81.2504]]},ca:{name:"Canada",bbox:[[-140.99778,41.675105],[-52.648099,83.23324]]}};var n=r;var a={};var l={de:"Suche",it:"Ricerca",en:"Search",nl:"Zoeken",fr:"Chercher",ca:"Cerca",he:"לחפש",ja:"サーチ",lv:"Meklēt",pt:"Procurar",sr:"Претрага",zh:"搜索",cs:"Vyhledávání",hu:"Keresés",ka:"ძიება",nb:"Søke",sk:"Vyhľadávanie",th:"ค้นหา",fi:"Hae",is:"Leita",ko:"수색",pl:"Szukaj",sl:"Iskanje",fa:"جستجو",ru:"Поиск"};a={placeholder:l};var h=a;var p="default"in e?e.default:e;var c="default"in t?t.default:t;var u="default"in i?i.default:i;var d="default"in s?s.default:s;var g="default"in o?o.default:o;var m={};var f=p;var _=c;var v=u;var y=d.EventEmitter;var b=n;var E=h;var x=g;
/**
 * A geocoder component that works with maplibre
 * @class MaplibreGeocoder
 * @param {Object} geocoderApi Any geocoder api that supports the functions reverseGeocode and forwardGeocode and returns a response which includes a FeatureCollection of results
 * @param {Object} options
 * @param {Object} [options.maplibregl] A [maplibre-gl](https://github.com/maplibre/maplibre-gl-js) instance to use when creating [Markers](https://maplibre.org/maplibre-gl-js-docs/api/markers/#marker). Required if `options.marker` is `true`.
 * @param {Number} [options.zoom=16] On geocoded result what zoom level should the map animate to when a `bbox` isn't found in the response. If a `bbox` is found the map will fit to the `bbox`.
 * @param {Boolean|Object} [options.flyTo=true] If `false`, animating the map to a selected result is disabled. If `true`, animating the map will use the default animation parameters. If an object, it will be passed as `options` to the map [`flyTo`](https://maplibre.org/maplibre-gl-js-docs/api/map/#map#flyto) or [`fitBounds`](https://maplibre.org/maplibre-gl-js-docs/api/map/#map#fitbounds) method providing control over the animation of the transition.
 * @param {String} [options.placeholder=Search] Override the default placeholder attribute value.
 * @param {Object} [options.proximity] a proximity argument: this is
 * a geographical point given as an object with `latitude` and `longitude`
 * properties. Search results closer to this point will be given
 * higher priority.
 * @param {Boolean} [options.trackProximity=true] If `true`, the geocoder proximity will automatically update based on the map view.
 * @param {Boolean} [options.collapsed=false] If `true`, the geocoder control will collapse until hovered or in focus.
 * @param {Boolean} [options.clearAndBlurOnEsc=false] If `true`, the geocoder control will clear it's contents and blur when user presses the escape key.
 * @param {Boolean} [options.clearOnBlur=false] If `true`, the geocoder control will clear its value when the input blurs.
 * @param {Array} [options.bbox] a bounding box argument: this is
 * a bounding box given as an array in the format `[minX, minY, maxX, maxY]`.
 * Search results will be limited to the bounding box.
 * @param {string} [options.countries] a comma separated list of country codes to
 * limit results to specified country or countries.
 * @param {string} [options.types] a comma seperated list of types that filter
 * results to match those specified. See https://docs.mapbox.com/api/search/#data-types
 * for available types.
 * If reverseGeocode is enabled, you should specify one type. If you configure more than one type, the first type will be used.
 * @param {Number} [options.minLength=2] Minimum number of characters to enter before results are shown.
 * @param {Number} [options.limit=5] Maximum number of results to show.
 * @param {string} [options.language] Specify the language to use for response text and query result weighting. Options are IETF language tags comprised of a mandatory ISO 639-1 language code and optionally one or more IETF subtags for country or script. More than one value can also be specified, separated by commas. Defaults to the browser's language settings.
 * @param {Function} [options.filter] A function which accepts a Feature in the [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) format to filter out results from the Geocoding API response before they are included in the suggestions list. Return `true` to keep the item, `false` otherwise.
 * @param {Function} [options.localGeocoder] A function accepting the query string which performs local geocoding to supplement results from the Maplibre Geocoding API. Expected to return an Array of GeoJSON Features in the [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) format.
 * @param {Function} [options.externalGeocoder] A function accepting the query string, current features list, and geocoder options which performs geocoding to supplement results from the Maplibre Geocoding API. Expected to return a Promise which resolves to an Array of GeoJSON Features in the [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) format.
 * @param {distance|score} [options.reverseMode=distance] - Set the factors that are used to sort nearby results.
 * @param {boolean} [options.reverseGeocode=false] If `true`, enable reverse geocoding mode. In reverse geocoding, search input is expected to be coordinates in the form `lat, lon`, with suggestions being the reverse geocodes.
 * @param {Boolean} [options.enableEventLogging=true] Allow Maplibre to collect anonymous usage statistics from the plugin.
 * @param {Boolean|Object} [options.marker=true]  If `true`, a [Marker](https://maplibre.org/maplibre-gl-js-docs/api/markers/#marker) will be added to the map at the location of the user-selected result using a default set of Marker options.  If the value is an object, the marker will be constructed using these options. If `false`, no marker will be added to the map. Requires that `options.maplibregl` also be set.
 * @param {Boolean|Object} [options.popup=true]  If `true`, a [Popup](https://maplibre.org/maplibre-gl-js-docs/api/markers/#popup) will be added to the map when clicking on a marker using a default set of popup options.  If the value is an object, the popup will be constructed using these options. If `false`, no popup will be added to the map. Requires that `options.maplibregl` also be set.
 * @param {Boolean|Object} [options.showResultMarkers=true]  If `true`, [Markers](https://maplibre.org/maplibre-gl-js-docs/api/markers/#marker) will be added to the map at the location the top results for the query.   If the value is an object, the marker will be constructed using these options. If `false`, no marker will be added to the map. Requires that `options.maplibregl` also be set.
 * @param {Function} [options.render] A function that specifies how the results should be rendered in the dropdown menu. This function should accepts a single [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) object as input and return a string. Any HTML in the returned string will be rendered.
 * @param {Function} [options.popupRender] A function that specifies how the results should be rendered in the popup menu. This function should accept a single [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) object as input and return a string. Any HTML in the returned string will be rendered.
 * @param {Function} [options.getItemValue] A function that specifies how the selected result should be rendered in the search bar. This function should accept a single [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) object as input and return a string. HTML tags in the output string will not be rendered. Defaults to `(item) => item.place_name`.
 * @param {Boolean} [options.localGeocoderOnly=false] If `true`, indicates that the `localGeocoder` results should be the only ones returned to the user. If `false`, indicates that the `localGeocoder` results should be combined with those from the Maplibre API with the `localGeocoder` results ranked higher.
 * @param {Boolean} [options.showResultsWhileTyping=false] If `false`, indicates that search will only occur on enter key press. If `true`, indicates that the Geocoder will search on the input box being updated above the minLength option.
 * @param {Number} [options.debounceSearch=200] Sets the amount of time, in milliseconds, to wait before querying the server when a user types into the Geocoder input box. This parameter may be useful for reducing the total number of API calls made for a single query.
 * @example
 *
 * var GeoApi = {
 *   forwardGeocode: (config) => { return { features: [] } },
 *   reverseGeocode: (config) => { return { features: [] } }
 *   getSuggestions: (config) => { return { suggestions: string[] }}
 *   getByPlaceId: (config) => { return { suggestions: {text: string, placeId?: string}[] }}
 * }
 * var geocoder = new MaplibreGeocoder(GeoApi, {});
 * map.addControl(geocoder);
 * @return {MaplibreGeocoder} `this`
 *
 */function MaplibreGeocoder(e,t){this._eventEmitter=new y;this.options=v({},this.options,t);this.inputString="";this.fresh=true;this.lastSelected=null;this.geocoderApi=e}MaplibreGeocoder.prototype={options:{zoom:16,flyTo:true,trackProximity:true,showResultsWhileTyping:false,minLength:2,reverseGeocode:false,limit:5,enableEventLogging:true,marker:true,popup:false,maplibregl:null,collapsed:false,clearAndBlurOnEsc:false,clearOnBlur:false,getItemValue:function(e){return void 0!==e.text?e.text:e.place_name},render:function(e){if(e.geometry){var t=e.place_name.split(",");return'<div class="mapboxgl-ctrl-geocoder--result maplibregl-ctrl-geocoder--result"><svg class="mapboxgl-ctrl-geocoder--result-icon maplibre-ctrl-geocoder--result-icon" viewBox="0 0 24 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 0C5.36571 0 0 5.38676 0 12.0471C0 21.0824 12 32 12 32C12 32 24 21.0824 24 12.0471C24 5.38676 18.6343 0 12 0ZM12 16.3496C9.63428 16.3496 7.71429 14.4221 7.71429 12.0471C7.71429 9.67207 9.63428 7.74454 12 7.74454C14.3657 7.74454 16.2857 9.67207 16.2857 12.0471C16.2857 14.4221 14.3657 16.3496 12 16.3496Z" fill="#687078"/></svg><div><div class="mapboxgl-ctrl-geocoder--result-title maplibregl-ctrl-geocoder--result-title">'+t[0]+'</div><div class="mapboxgl-ctrl-geocoder--result-address maplibregl-ctrl-geocoder--result-address">'+t.splice(1,t.length).join(",")+"</div></div></div>"}var i=e.text;var s=i.toLowerCase().indexOf(this.query.toLowerCase());var o=this.query.length;var r=i.substring(0,s);var n=i.substring(s,s+o);var a=i.substring(s+o);return'<div class="mapboxgl-ctrl-geocoder--suggestion maplibregl-ctrl-geocoder--suggestion"><svg class="mapboxgl-ctrl-geocoder--suggestion-icon maplibre-ctrl-geocoder--suggestion-icon" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><path d="M22.8702 20.1258H21.4248L20.9125 19.6318C22.7055 17.546 23.785 14.8382 23.785 11.8925C23.785 5.32419 18.4608 0 11.8925 0C5.32419 0 0 5.32419 0 11.8925C0 18.4608 5.32419 23.785 11.8925 23.785C14.8382 23.785 17.546 22.7055 19.6318 20.9125L20.1258 21.4248V22.8702L29.2739 32L32 29.2739L22.8702 20.1258ZM11.8925 20.1258C7.33676 20.1258 3.65923 16.4483 3.65923 11.8925C3.65923 7.33676 7.33676 3.65923 11.8925 3.65923C16.4483 3.65923 20.1258 7.33676 20.1258 11.8925C20.1258 16.4483 16.4483 20.1258 11.8925 20.1258Z" fill="#687078"/></svg><div class="mapboxgl-ctrl-geocoder--suggestion-info maplibregl-ctrl-geocoder--suggestion-info"><div class="mapboxgl-ctrl-geocoder--suggestion-title maplibregl-ctrl-geocoder--suggestion-title">'+r+'<span class="mapboxgl-ctrl-geocoder--suggestion-match maplibregl-ctrl-geocoder--suggestion-match">'+n+"</span>"+a+"</div></div></div>"},popupRender:function(e){var t=e.place_name.split(",");return'<div class="mapboxgl-ctrl-geocoder--suggestion maplibre-ctrl-geocoder--suggestion popup-suggestion"><div class="mapboxgl-ctrl-geocoder--suggestion-title maplibre-ctrl-geocoder--suggestion-title popup-suggestion-title">'+t[0]+'</div><div class="mapboxgl-ctrl-geocoder--suggestion-address maplibre-ctrl-geocoder--suggestion-address popup-suggestion-address">'+t.splice(1,t.length).join(",")+"</div></div>"},showResultMarkers:true,debounceSearch:200},
/**
   * Add the geocoder to a container. The container can be either a `maplibregl.Map`, an `HTMLElement` or a CSS selector string.
   *
   * If the container is a [`maplibregl.Map`](https://maplibre.org/maplibre-gl-js-docs/api/map/#map), this function will behave identically to [`Map.addControl(geocoder)`](https://maplibre.org/maplibre-gl-js-docs/api/map/#map#addcontrol).
   * If the container is an instance of [`HTMLElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement), then the geocoder will be appended as a child of that [`HTMLElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement).
   * If the container is a [CSS selector string](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors), the geocoder will be appended to the element returned from the query.
   *
   * This function will throw an error if the container is none of the above.
   * It will also throw an error if the referenced HTML element cannot be found in the `document.body`.
   *
   * For example, if the HTML body contains the element `<div id='geocoder-container'></div>`, the following script will append the geocoder to `#geocoder-container`:
   *
   * ```javascript
   * var GeoApi = {
   *   forwardGeocode: (config) => { return { features: [] } },
   *   reverseGeocode: (config) => { return { features: [] } }
   * }
   * var geocoder = new MaplibreGeocoder(GeoAPI, {});
   * geocoder.addTo('#geocoder-container');
   * ```
   * @param {String|HTMLElement|maplibregl.Map} container A reference to the container to which to add the geocoder
   */
addTo:function(e){function addToExistingContainer(e,t){if(!document.body.contains(t))throw new Error("Element provided to #addTo() exists, but is not in the DOM");var i=e.onAdd();t.appendChild(i)}if(e._controlContainer)e.addControl(this);else if(e instanceof HTMLElement)addToExistingContainer(this,e);else{if("string"!=typeof e)throw new Error("Error: addTo must be a maplibre-gl-js map, an html element, or a CSS selector query for a single html element");var t=document.querySelectorAll(e);if(0===t.length)throw new Error("Element ",e,"not found.");if(t.length>1)throw new Error("Geocoder can only be added to a single html element");addToExistingContainer(this,t[0])}},onAdd:function(e){e&&"string"!=typeof e&&(this._map=e);this.setLanguage();if(this.options.localGeocoderOnly&&!this.options.localGeocoder)throw new Error("A localGeocoder function must be specified to use localGeocoderOnly mode");this._onChange=this._onChange.bind(this);this._onKeyDown=this._onKeyDown.bind(this);this._onPaste=this._onPaste.bind(this);this._onBlur=this._onBlur.bind(this);this._showButton=this._showButton.bind(this);this._hideButton=this._hideButton.bind(this);this._onQueryResult=this._onQueryResult.bind(this);this.clear=this.clear.bind(this);this._updateProximity=this._updateProximity.bind(this);this._collapse=this._collapse.bind(this);this._unCollapse=this._unCollapse.bind(this);this._clear=this._clear.bind(this);this._clearOnBlur=this._clearOnBlur.bind(this);var t=this.container=document.createElement("div");t.className="mapboxgl-ctrl-geocoder mapboxgl-ctrl maplibregl-ctrl-geocoder maplibregl-ctrl";var i=this.createIcon("search",'<path d="M7.4 2.5c-2.7 0-4.9 2.2-4.9 4.9s2.2 4.9 4.9 4.9c1 0 1.8-.2 2.5-.8l3.7 3.7c.2.2.4.3.8.3.7 0 1.1-.4 1.1-1.1 0-.3-.1-.5-.3-.8L11.4 10c.4-.8.8-1.6.8-2.5.1-2.8-2.1-5-4.8-5zm0 1.6c1.8 0 3.2 1.4 3.2 3.2s-1.4 3.2-3.2 3.2-3.3-1.3-3.3-3.1 1.4-3.3 3.3-3.3z"/>');this._inputEl=document.createElement("input");this._inputEl.type="text";this._inputEl.className="mapboxgl-ctrl-geocoder--input maplibregl-ctrl-geocoder--input";this.setPlaceholder();if(this.options.collapsed){this._collapse();this.container.addEventListener("mouseenter",this._unCollapse);this.container.addEventListener("mouseleave",this._collapse);this._inputEl.addEventListener("focus",this._unCollapse)}(this.options.collapsed||this.options.clearOnBlur)&&this._inputEl.addEventListener("blur",this._onBlur);this._inputEl.addEventListener("keydown",_(this._onKeyDown,this.options.debounceSearch));this._inputEl.addEventListener("paste",this._onPaste);this._inputEl.addEventListener("change",this._onChange);this.container.addEventListener("mouseenter",this._showButton);this.container.addEventListener("mouseleave",this._hideButton);var s=document.createElement("div");s.classList.add("mapboxgl-ctrl-geocoder--pin-right","maplibregl-ctrl-geocoder--pin-right");this._clearEl=document.createElement("button");this._clearEl.setAttribute("aria-label","Clear");this._clearEl.addEventListener("click",this.clear);this._clearEl.className="mapboxgl-ctrl-geocoder--button maplibregl-ctrl-geocoder--button";var o=this.createIcon("close",'<path d="M3.8 2.5c-.6 0-1.3.7-1.3 1.3 0 .3.2.7.5.8L7.2 9 3 13.2c-.3.3-.5.7-.5 1 0 .6.7 1.3 1.3 1.3.3 0 .7-.2 1-.5L9 10.8l4.2 4.2c.2.3.7.3 1 .3.6 0 1.3-.7 1.3-1.3 0-.3-.2-.7-.3-1l-4.4-4L15 4.6c.3-.2.5-.5.5-.8 0-.7-.7-1.3-1.3-1.3-.3 0-.7.2-1 .3L9 7.1 4.8 2.8c-.3-.1-.7-.3-1-.3z"/>');this._clearEl.appendChild(o);this._loadingEl=this.createIcon("loading",'<path fill="#333" d="M4.4 4.4l.8.8c2.1-2.1 5.5-2.1 7.6 0l.8-.8c-2.5-2.5-6.7-2.5-9.2 0z"/><path opacity=".1" d="M12.8 12.9c-2.1 2.1-5.5 2.1-7.6 0-2.1-2.1-2.1-5.5 0-7.7l-.8-.8c-2.5 2.5-2.5 6.7 0 9.2s6.6 2.5 9.2 0 2.5-6.6 0-9.2l-.8.8c2.2 2.1 2.2 5.6 0 7.7z"/>');s.appendChild(this._clearEl);s.appendChild(this._loadingEl);t.appendChild(i);t.appendChild(this._inputEl);t.appendChild(s);this._typeahead=new f(this._inputEl,[],{filter:false,minLength:this.options.minLength,limit:this.options.limit,noInitialSelection:true});this.setRenderFunction(this.options.render);this._typeahead.getItemValue=this.options.getItemValue;this.mapMarker=null;this.resultMarkers=[];this._handleMarker=this._handleMarker.bind(this);this._handleResultMarkers=this._handleResultMarkers.bind(this);if(this._map){if(this.options.trackProximity){this._updateProximity();this._map.on("moveend",this._updateProximity)}this._maplibregl=this.options.maplibregl;if(!this._maplibregl&&this.options.marker){console.error("No maplibregl detected in options. Map markers are disabled. Please set options.maplibregl.");this.options.marker=false}}return t},createIcon:function(e,t){var i=document.createElementNS("http://www.w3.org/2000/svg","svg");i.setAttribute("class","mapboxgl-ctrl-geocoder--icon mapboxgl-ctrl-geocoder--icon-"+e+" maplibregl-ctrl-geocoder--icon maplibregl-ctrl-geocoder--icon-"+e);i.setAttribute("viewBox","0 0 18 18");i.setAttribute("xml:space","preserve");i.setAttribute("width",18);i.setAttribute("height",18);if("innerHTML"in i)i.innerHTML=t;else{var s=document.createElement("div");s.innerHTML="<svg>"+t.valueOf().toString()+"</svg>";var o=s.firstChild,r=o.firstChild;i.appendChild(r)}return i},onRemove:function(){this.container.parentNode.removeChild(this.container);this.options.trackProximity&&this._map&&this._map.off("moveend",this._updateProximity);this._removeMarker();this._map=null;return this},_onPaste:function(e){var t=(e.clipboardData||window.clipboardData).getData("text");t.length>=this.options.minLength&&this.options.showResultsWhileTyping&&this._geocode(t)},_onKeyDown:function(e){var t=27,i=9;if(e.keyCode===t&&this.options.clearAndBlurOnEsc){this._clear(e);return this._inputEl.blur()}var s=e.target&&e.target.shadowRoot?e.target.shadowRoot.activeElement:e.target;var o=s?s.value:"";if(!o){this.fresh=true;e.keyCode!==i&&this.clear(e);return this._clearEl.style.display="none"}if(!e.metaKey&&-1===[i,t,37,39,38,40].indexOf(e.keyCode)){if(13===e.keyCode){if(this.options.showResultsWhileTyping){null==this._typeahead.selected&&this.geocoderApi.getSuggestions?this._geocode(s.value,true):null==this._typeahead.selected&&this.options.showResultMarkers&&this._fitBoundsForMarkers();return}this._typeahead.selected||this._geocode(s.value)}s.value.length>=this.options.minLength&&this.options.showResultsWhileTyping&&this._geocode(s.value)}},_showButton:function(){this._inputEl.value.length>0&&(this._clearEl.style.display="block")},_hideButton:function(){this._typeahead.selected&&(this._clearEl.style.display="none")},_onBlur:function(e){this.options.clearOnBlur&&this._clearOnBlur(e);this.options.collapsed&&this._collapse()},_onChange:function(){var e=this._typeahead.selected;if(e&&!e.geometry)e.placeId?this._geocode(e.placeId,true,true):this._geocode(e.text,true);else if(e&&JSON.stringify(e)!==this.lastSelected){this._clearEl.style.display="none";if(this.options.flyTo){var t;this._removeResultMarkers();if(e.properties&&b[e.properties.short_code]){t=v({},this.options.flyTo);this._map&&this._map.fitBounds(b[e.properties.short_code].bbox,t)}else if(e.bbox){var i=e.bbox;t=v({},this.options.flyTo);this._map&&this._map.fitBounds([[i[0],i[1]],[i[2],i[3]]],t)}else{var s={zoom:this.options.zoom};t=v({},s,this.options.flyTo);e.center?t.center=e.center:e.geometry&&e.geometry.type&&"Point"===e.geometry.type&&e.geometry.coordinates&&(t.center=e.geometry.coordinates);this._map&&this._map.flyTo(t)}}this.options.marker&&this._maplibregl&&this._handleMarker(e);this._inputEl.focus();this._inputEl.scrollLeft=0;this._inputEl.setSelectionRange(0,0);this.lastSelected=JSON.stringify(e);this._typeahead.selected=null;this._eventEmitter.emit("result",{result:e})}},_getConfigForRequest:function(){var e=["bbox","limit","proximity","countries","types","language","reverseMode"];var t=this;var i=e.reduce((function(e,i){if(t.options[i]){["countries","types","language"].indexOf(i)>-1?e[i]=t.options[i].split(/[\s,]+/):e[i]=t.options[i];"proximity"===i&&t.options[i]&&"number"===typeof t.options[i].longitude&&"number"===typeof t.options[i].latitude&&(e[i]=[t.options[i].longitude,t.options[i].latitude])}return e}),{});return i},_geocode:function(e,t,i){this._loadingEl.style.display="block";this._eventEmitter.emit("loading",{query:e});this.inputString=e;var s=null;var o=this._getConfigForRequest();var r;if(this.options.localGeocoderOnly)r=Promise.resolve();else if(this.options.reverseGeocode&&/(-?\d+\.?\d*)[, ]+(-?\d+\.?\d*)[ ]*$/.test(e)){var n=e.split(/[\s(,)?]+/).map((function(e){return parseFloat(e,10)})).reverse();o.types?[o.types[0]]:["poi"];o=v(o,{query:n,limit:1});"proximity"in o&&delete o.proximity;r=this.geocoderApi.reverseGeocode(o)}else{o=v(o,{query:e});r=this.geocoderApi.getSuggestions?t?this.geocoderApi.searchByPlaceId&&i?this.geocoderApi.searchByPlaceId(o):this.geocoderApi.forwardGeocode(o):this.geocoderApi.getSuggestions(o):this.geocoderApi.forwardGeocode(o)}var a=[];if(this.options.localGeocoder){a=this.options.localGeocoder(e);a||(a=[])}var l=[];r.catch(function(e){s=e}.bind(this)).then(function(t){this._loadingEl.style.display="none";var i={};i=t||{type:"FeatureCollection",features:[]};i.config=o;this.fresh&&(this.fresh=false);i.features=i.features?a.concat(i.features):a;if(this.options.externalGeocoder){l=this.options.externalGeocoder(e,i.features,o)||[];return l.then((function(e){i.features=i.features?e.concat(i.features):e;return i}),(function(){return i}))}return i}.bind(this)).then(function(e){if(s)throw s;this.options.filter&&e.features.length&&(e.features=e.features.filter(this.options.filter));var i=[];i=e.suggestions?e.suggestions:e.place?[e.place]:e.features;if(i.length){this._clearEl.style.display="block";this._typeahead.update(i);(!this.options.showResultsWhileTyping||t)&&this.options.showResultMarkers&&(e.features.length>0||e.place)&&this._fitBoundsForMarkers();this._eventEmitter.emit("results",e)}else{this._clearEl.style.display="none";this._typeahead.selected=null;this._renderNoResults();this._eventEmitter.emit("results",e)}}.bind(this)).catch(function(e){this._loadingEl.style.display="none";if(a.length&&this.options.localGeocoder||l.length&&this.options.externalGeocoder){this._clearEl.style.display="block";this._typeahead.update(a)}else{this._clearEl.style.display="none";this._typeahead.selected=null;this._renderError()}this._eventEmitter.emit("results",{features:a});this._eventEmitter.emit("error",{error:e})}.bind(this));return r},
/**
   * Shared logic for clearing input
   * @param {Event} [ev] the event that triggered the clear, if available
   * @private
   *
   */
_clear:function(e){e&&e.preventDefault();this._inputEl.value="";this._typeahead.selected=null;this._typeahead.clear();this._onChange();this._clearEl.style.display="none";this._removeMarker();this._removeResultMarkers();this.lastSelected=null;this._eventEmitter.emit("clear");this.fresh=true},
/**
   * Clear and then focus the input.
   * @param {Event} [ev] the event that triggered the clear, if available
   *
   */
clear:function(e){this._clear(e);this._inputEl.focus()},
/**
   * Clear the input, without refocusing it. Used to implement clearOnBlur
   * constructor option.
   * @param {Event} [ev] the blur event
   * @private
   */
_clearOnBlur:function(e){var t=this;e.relatedTarget&&t._clear(e)},_onQueryResult:function(e){var t=e;if(t.features.length){var i=t.features[0];this._typeahead.selected=i;this._inputEl.value=i.place_name;this._onChange()}},_updateProximity:function(){if(this._map)if(this._map.getZoom()>9){var e=this._map.getCenter().wrap();this.setProximity({longitude:e.lng,latitude:e.lat})}else this.setProximity(null)},_collapse:function(){this._inputEl.value||this._inputEl===document.activeElement||this.container.classList.add("mapboxgl-ctrl-geocoder--collapsed","maplibregl-ctrl-geocoder--collapsed")},_unCollapse:function(){this.container.classList.remove("mapboxgl-ctrl-geocoder--collapsed","maplibregl-ctrl-geocoder--collapsed")},
/**
   * Set & query the input
   * @param {string} searchInput location name or other search input
   * @returns {MaplibreGeocoder} this
   */
query:function(e){this._geocode(e).then(this._onQueryResult);return this},_renderError:function(){var e="<div class='mapbox-gl-geocoder--error maplibre-gl-geocoder--error'>There was an error reaching the server</div>";this._renderMessage(e)},_renderNoResults:function(){var e="<div class='mapbox-gl-geocoder--error mapbox-gl-geocoder--no-results maplibre-gl-geocoder--error maplibre-gl-geocoder--no-results'>No results found</div>";this._renderMessage(e)},_renderMessage:function(e){this._typeahead.update([]);this._typeahead.selected=null;this._typeahead.clear();this._typeahead.renderError(e)},
/**
   * Get the text to use as the search bar placeholder
   *
   * If placeholder is provided in options, then use options.placeholder
   * Otherwise, if language is provided in options, then use the localized string of the first language if available
   * Otherwise use the default
   *
   * @returns {String} the value to use as the search bar placeholder
   * @private
   */
_getPlaceholderText:function(){if(this.options.placeholder)return this.options.placeholder;if(this.options.language){var e=this.options.language.split(",")[0];var t=x.language(e);var i=E.placeholder[t];if(i)return i}return"Search"},
/**
   * Fits the map to the current bounds for the searched results
   *
   * @returns {MaplibreGeocoder} this
   * @private
   */
_fitBoundsForMarkers:function(){if(!(this._typeahead.data.length<1)){var e=this._typeahead.data.filter((function(e){return"string"!==typeof e})).slice(0,this.options.limit);this._clearEl.style.display="none";if(this.options.flyTo&&this._maplibregl&&this._map){var t={padding:100};var i=v({},t,this.options.flyTo);var s=new this._maplibregl.LngLatBounds;e.forEach((function(e){s.extend(e.geometry.coordinates)}));this._map.fitBounds(s.toArray(),i)}e.length>0&&this._maplibregl&&this._handleResultMarkers(e);return this}},
/**
   * Set input
   * @param {string} searchInput location name or other search input
   * @returns {MaplibreGeocoder} this
   */
setInput:function(e){this._inputEl.value=e;this._typeahead.selected=null;this._typeahead.clear();e.length>=this.options.minLength&&this.options.showResultsWhileTyping&&this._geocode(e);return this},
/**
   * Set proximity
   * @param {Object} proximity The new `options.proximity` value. This is a geographical point given as an object with `latitude` and `longitude` properties.
   * @returns {MaplibreGeocoder} this
   */
setProximity:function(e){this.options.proximity=e;return this},
/**
   * Get proximity
   * @returns {Object} The geocoder proximity
   */
getProximity:function(){return this.options.proximity},
/**
   * Set the render function used in the results dropdown
   * @param {Function} fn The function to use as a render function. This function accepts a single [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) object as input and returns a string.
   * @returns {MaplibreGeocoder} this
   */
setRenderFunction:function(e){e&&"function"==typeof e&&(this._typeahead.render=e);return this},
/**
   * Get the function used to render the results dropdown
   *
   * @returns {Function} the render function
   */
getRenderFunction:function(){return this._typeahead.render},
/**
   * Get the language to use in UI elements and when making search requests
   *
   * Look first at the explicitly set options otherwise use the browser's language settings
   * @param {String} language Specify the language to use for response text and query result weighting. Options are IETF language tags comprised of a mandatory ISO 639-1 language code and optionally one or more IETF subtags for country or script. More than one value can also be specified, separated by commas.
   * @returns {MaplibreGeocoder} this
   */
setLanguage:function(e){var t=navigator.language||navigator.userLanguage||navigator.browserLanguage;this.options.language=e||this.options.language||t;return this},
/**
   * Get the language to use in UI elements and when making search requests
   * @returns {String} The language(s) used by the plugin, if any
   */
getLanguage:function(){return this.options.language},
/**
   * Get the zoom level the map will move to when there is no bounding box on the selected result
   * @returns {Number} the map zoom
   */
getZoom:function(){return this.options.zoom},
/**
   * Set the zoom level
   * @param {Number} zoom The zoom level that the map should animate to when a `bbox` isn't found in the response. If a `bbox` is found the map will fit to the `bbox`.
   * @returns {MaplibreGeocoder} this
   */
setZoom:function(e){this.options.zoom=e;return this},
/**
   * Get the parameters used to fly to the selected response, if any
   * @returns {Boolean|Object} The `flyTo` option
   */
getFlyTo:function(){return this.options.flyTo},
/**
   * Set the flyTo options
   * @param {Boolean|Object} flyTo If false, animating the map to a selected result is disabled. If true, animating the map will use the default animation parameters. If an object, it will be passed as `options` to the map [`flyTo`](https://maplibre.org/maplibre-gl-js-docs/api/map/#map#flyto) or [`fitBounds`](https://maplibre.org/maplibre-gl-js-docs/api/map/#map#fitbounds) method providing control over the animation of the transition.
   */
setFlyTo:function(e){this.options.flyTo=e;return this},
/**
   * Get the value of the placeholder string
   * @returns {String} The input element's placeholder value
   */
getPlaceholder:function(){return this.options.placeholder},
/**
   * Set the value of the input element's placeholder
   * @param {String} placeholder the text to use as the input element's placeholder
   * @returns {MaplibreGeocoder} this
   */
setPlaceholder:function(e){this.placeholder=e||this._getPlaceholderText();this._inputEl.placeholder=this.placeholder;this._inputEl.setAttribute("aria-label",this.placeholder);return this},
/**
   * Get the bounding box used by the plugin
   * @returns {Array<Number>} the bounding box, if any
   */
getBbox:function(){return this.options.bbox},
/**
   * Set the bounding box to limit search results to
   * @param {Array<Number>} bbox a bounding box given as an array in the format [minX, minY, maxX, maxY].
   * @returns {MaplibreGeocoder} this
   */
setBbox:function(e){this.options.bbox=e;return this},
/**
   * Get a list of the countries to limit search results to
   * @returns {String} a comma separated list of countries to limit to, if any
   */
getCountries:function(){return this.options.countries},
/**
   * Set the countries to limit search results to
   * @param {String} countries a comma separated list of countries to limit to
   * @returns {MaplibreGeocoder} this
   */
setCountries:function(e){this.options.countries=e;return this},
/**
   * Get a list of the types to limit search results to
   * @returns {String} a comma separated list of types to limit to
   */
getTypes:function(){return this.options.types},
/**
   * Set the types to limit search results to
   * @param {String} countries a comma separated list of types to limit to
   * @returns {MaplibreGeocoder} this
   */
setTypes:function(e){this.options.types=e;return this},
/**
   * Get the minimum number of characters typed to trigger results used in the plugin
   * @returns {Number} The minimum length in characters before a search is triggered
   */
getMinLength:function(){return this.options.minLength},
/**
   * Set the minimum number of characters typed to trigger results used by the plugin
   * @param {Number} minLength the minimum length in characters
   * @returns {MaplibreGeocoder} this
   */
setMinLength:function(e){this.options.minLength=e;this._typeahead&&(this._typeahead.options.minLength=e);return this},
/**
   * Get the limit value for the number of results to display used by the plugin
   * @returns {Number} The limit value for the number of results to display used by the plugin
   */
getLimit:function(){return this.options.limit},
/**
   * Set the limit value for the number of results to display used by the plugin
   * @param {Number} limit the number of search results to return
   * @returns {MaplibreGeocoder}
   */
setLimit:function(e){this.options.limit=e;this._typeahead&&(this._typeahead.options.limit=e);return this},
/**
   * Get the filter function used by the plugin
   * @returns {Function} the filter function
   */
getFilter:function(){return this.options.filter},
/**
   * Set the filter function used by the plugin.
   * @param {Function} filter A function which accepts a Feature in the [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) format to filter out results from the Geocoding API response before they are included in the suggestions list. Return `true` to keep the item, `false` otherwise.
   * @returns {MaplibreGeocoder} this
   */
setFilter:function(e){this.options.filter=e;return this},
/**
   * Set the geocoding api used by the plugin.
   * @param {Object} geocoderApi An API which contains reverseGeocode and forwardGeocode functions to be used by this plugin
   * @param {Function} geocoderApi.forwardGeocode Forward geocode function should return an object including a collection of Features in [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) format
   * @param {Object} geocoderApi.forwardGeocode.config Query parameters
   * @param {String} geocoderApi.forwardGeocode.config.query Search query string
   * @param {Number} geocoderApi.forwardGeocode.config.limit Number of results to limit by
   * @param {Array} geocoderApi.forwardGeocode.config.bbox a bounding box given as an array in the format `[minX, minY, maxX, maxY]`. Search results will be limited to the bounding box.
   * @param {Object} geocoderApi.forwardGeocode.config.proximity a geographical point given as an object with `latitude` and `longitude` properties. Search results closer to this point will be given higher priority.
   * @param {Array} geocoderApi.forwardGeocode.config.countries a comma separated list of country codes to limit results to specified country or countries.
   * @param {Array} geocoderApi.forwardGeocode.config.types a comma seperated list of types that filter results to match those specified. See https://docs.mapbox.com/api/search/#data-types for available types. If reverseGeocode is enabled, you should specify one type. If you configure more than one type, the first type will be used.
   * @param {String} geocoderApi.forwardGeocode.config.language Specify the language to use for response text and query result weighting. Options are IETF language tags comprised of a mandatory ISO 639-1 language code and optionally one or more IETF subtags for country or script. More than one value can also be specified, separated by commas. Defaults to the browser's language settings.
   * @param {distance|score} geocoderApi.forwardGeocode.config.reverseMode Set the factors that are used to sort nearby results.
   *
   * @param {Function} geocoderApi.reverseGeocode Reverse geocode function should return an object including a collection of Features in [Carmen GeoJSON](https://github.com/mapbox/carmen/blob/master/carmen-geojson.md) format
   * @param {Object} geocoderApi.reverseGeocode.config Query parameters
   * @param {Object} geocoderApi.reverseGeocode.config.query Search query coordinates
   * @param {Number} geocoderApi.reverseGeocode.config.limit Number of results to limit by
   * @param {Array} geocoderApi.reverseGeocode.config.bbox a bounding box given as an array in the format `[minX, minY, maxX, maxY]`. Search results will be limited to the bounding box.
   * @param {Object} geocoderApi.reverseGeocode.config.proximity a geographical point given as an object with `latitude` and `longitude` properties. Search results closer to this point will be given higher priority.
   * @param {Array} geocoderApi.reverseGeocode.config.countries a comma separated list of country codes to limit results to specified country or countries.
   * @param {Array} geocoderApi.reverseGeocode.config.types a comma seperated list of types that filter results to match those specified. See https://docs.mapbox.com/api/search/#data-types for available types. If reverseGeocode is enabled, you should specify one type. If you configure more than one type, the first type will be used.
   * @param {String} geocoderApi.reverseGeocode.config.language Specify the language to use for response text and query result weighting. Options are IETF language tags comprised of a mandatory ISO 639-1 language code and optionally one or more IETF subtags for country or script. More than one value can also be specified, separated by commas. Defaults to the browser's language settings.
   * @param {distance|score} geocoderApi.reverseGeocode.config.reverseMode Set the factors that are used to sort nearby results.
   * @returns {MaplibreGeocoder} this
   */
setGeocoderApi:function(e){this.geocoderApi=e;return this},
/**
   * Get the geocoding endpoint the plugin is currently set to
   * @returns {Object} the geocoding API
   */
getGeocoderApi:function(){return this.geocoderApi},
/**
   * Handle the placement of a result marking the selected result
   * @private
   * @param {Object} selected the selected geojson feature
   * @returns {MaplibreGeocoder} this
   */
_handleMarker:function(e){if(this._map){this._removeMarker();var t={color:"#4668F2"};var i=v({},t,this.options.marker);this.mapMarker=new this._maplibregl.Marker(i);var s;if(this.options.popup){var o={};var r=v({},o,this.options.popup);s=new this._maplibregl.Popup(r).setHTML(this.options.popupRender(e))}if(e.center){this.mapMarker.setLngLat(e.center).addTo(this._map);this.options.popup&&this.mapMarker.setPopup(s)}else if(e.geometry&&e.geometry.type&&"Point"===e.geometry.type&&e.geometry.coordinates){this.mapMarker.setLngLat(e.geometry.coordinates).addTo(this._map);this.options.popup&&this.mapMarker.setPopup(s)}return this}},_removeMarker:function(){if(this.mapMarker){this.mapMarker.remove();this.mapMarker=null}},
/**
   * Handle the placement of a result marking the selected result
   * @private
   * @param {Object[]} results the top results to display on the map
   * @returns {MaplibreGeocoder} this
   */
_handleResultMarkers:function(e){if(this._map){this._removeResultMarkers();var t={color:"#4668F2"};var i=v({},t,this.options.showResultMarkers);e.forEach(function(e){if(this.options.showResultMarkers&&this.options.showResultMarkers.element){var t=this.options.showResultMarkers.element.cloneNode(true);i=v(i,{element:t})}var s=new this._maplibregl.Marker(v({},i,{element:t}));var o;if(this.options.popup){var r={};var n=v({},r,this.options.popup);o=new this._maplibregl.Popup(n).setHTML(this.options.popupRender(e))}if(e.center){s.setLngLat(e.center).addTo(this._map);this.options.popup&&s.setPopup(o)}else if(e.geometry&&e.geometry.type&&"Point"===e.geometry.type&&e.geometry.coordinates){s.setLngLat(e.geometry.coordinates).addTo(this._map);this.options.popup&&s.setPopup(o)}this.resultMarkers.push(s)}.bind(this));return this}},_removeResultMarkers:function(){if(this.resultMarkers&&this.resultMarkers.length>0){this.resultMarkers.forEach((function(e){e.remove()}));this.resultMarkers=[]}},
/**
   * Subscribe to events that happen within the plugin.
   * @param {String} type name of event. Available events and the data passed into their respective event objects are:
   *
   * - __clear__ `Emitted when the input is cleared`
   * - __loading__ `{ query } Emitted when the geocoder is looking up a query`
   * - __results__ `{ results } Fired when the geocoder returns a response`
   * - __result__ `{ result } Fired when input is set`
   * - __error__ `{ error } Error as string`
   * @param {Function} fn function that's called when the event is emitted.
   * @returns {MaplibreGeocoder} this;
   */
on:function(e,t){this._eventEmitter.on(e,t);return this},
/**
   * Remove an event
   * @returns {MaplibreGeocoder} this
   * @param {String} type Event name.
   * @param {Function} fn Function that should unsubscribe to the event emitted.
   */
off:function(e,t){this._eventEmitter.removeListener(e,t);return this}};m=MaplibreGeocoder;var k=m;export{k as default};

